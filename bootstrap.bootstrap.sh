#!/bin/bash

export KADOH_TRANSPORT=udp
echo "IPs:"
ip addr
node bin/bootstrap
exit

# Execute our stuff automatically
export KADOH_TRANSPORT=udp
cd /vagrant

echo "IPs:"
ifconfig|grep "inet "|grep addr

echo "Execing bootstrap"
rm -f ~/node.log
touch ~/node.log
killall node
node /vagrant/bin/bootstrap > ~/node.log &
sleep 2
cat ~/node.log
