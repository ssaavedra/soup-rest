/*jslint node: true, ass: true, white: true, nomen: true, todo: true, unparam:true, plusplus: true, vars: true */
/*jshint expr: true */
/*global describe: false, before: false, after: false, it: false */
"use strict";

var context = require('../lib/context'),
when = require('when'),
Identity = require('../lib/document/identity').Identity,
nodes = require('../lib/document/nodes');

/* Testing framework */

var chai = require("chai"),
chaiAsPromised = require("chai-as-promised"),
expect = chai.expect;

var Chunk = nodes.Chunk;

chai.use(chaiAsPromised);



describe ('Chunk', function () {

	describe ('With two instances of a chunk', function () {

		var storage, dao, buffer, firstChunk, secondChunk, ida, idb;

		var random_string = function (length) {
			var string = '', i, r,
			chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

			length = length || 32;

			for (i = 0; i < length; i++) {
				r = Math.floor (Math.random () * chars.length);
				string += chars.substring (r, r + 1);
			}
			return string;
		};

		var randomStringForTest = random_string (1024);

		before (function (done) {
			context.getProto ('nodeStorage', function (st) {
				storage = st;
				done();
			});
		});

		before (function (done) {
			context.getProto ('chunkDao', function (d) {
				dao = d;
				done();
			});
		});

		before (function () {
			buffer = new Buffer (randomStringForTest, 'utf8');
		});

		after (function () {

			var id = storage.getIdForEntity (new Chunk (null, buffer));

			return dao.fetch (id).then (function (chunk) {
				expect(chunk).not.to.be.null;
				var promise = dao.rm (chunk);

				return expect(promise).to.be.eventually.fulfilled;
			});
		});


		it ('exposes the needed API', function () {
			var achunk = new Chunk (null, new Buffer('test', 'utf8'));

			expect (Chunk).to.respondTo ('toJSON');
			expect (achunk).to.have.property ('content');
			expect (achunk.toJSON ()).not.to.be.null;
			expect (achunk.toJSON ()).to.have.a.property ('content');
		});


		it ('should initialize the first chunk', function () {
			var buf1 = new Buffer (randomStringForTest, 'utf8');
			firstChunk = new Chunk (null, buf1);
			expect (firstChunk).to.be.not.null;
			expect (firstChunk).be.an.instanceof (Chunk);
		});

		it ('should initialize the second chunk', function () {
			var buf2 = new Buffer (randomStringForTest, 'utf8');
			secondChunk = new Chunk (null, buf2);
			expect (secondChunk).to.be.not.null;
			expect (firstChunk).be.an.instanceof (Chunk);
		});

		it ('should have the same id when calculated', function () {
			ida = storage.getIdForEntity (firstChunk);
			idb = storage.getIdForEntity (secondChunk);

			expect (ida).to.equal(idb);
		});

		it ('can be saved', function () {
			var promise = dao.save (firstChunk);
			return expect (promise).to.be.eventually.fulfilled;
		});

		it ('can be obtained from the database', function () {
			var promise = dao.fetch (ida);

			return expect (promise).to.be.eventually.fulfilled;
		});
	});

});


describe ('Permanode', function () {
	var Permanode = nodes.Permanode;
	var dao;
	var identity;
	var service;

	var cleanup = [];

	before (function (finish) {
		context.getProto ('permanodeDao', function (i) {
			dao = i;
			expect (dao).not.to.be.undefined;
			finish ();
		});
	});

	before(function (finish) {
		context.getProto ('permanodeService', function (i) {
			service = i;
			expect (service).not.to.be.undefined;
			finish ();
		});
	});

	before (function (finish) {
		context.getProto ('identityService', function (is) {
			expect (is).not.to.be.undefined;
			expect (is).not.to.be.null;
			var promise = is.create (null);

			promise.then (function (i) {
				identity = i;

				expect (identity).to.respondTo ('toJSON');
				expect (identity).to.be.an.instanceof (Identity);
			}).done (finish);
		});

	});

	/* No need to persist the identity
	   after (function (done) {
	   context.getProto ('identityDao', function (dao) {
	   dao.rm (identity).then (done);
	   });
	   });
	*/

	after (function () {
		return when.all (cleanup.map (function (elt) {
			return dao.rm (elt);
		}));
	});

	it ('can be created', function () {
		var promise = service.create (identity.id);

		expect (promise).to.be.eventually.fulfilled;
		expect (promise).to.be.eventually.an.instanceof (Permanode);

		promise.then (function (e) { cleanup.push (e); });

		return promise;
	});

	it ('exposes a correct API', function () {
		expect (Permanode).to.respondTo ('toJSON');
		expect (Permanode).itself.to.respondTo ('create');
	});

	describe ('after persistence', function () {
		var permanode;


		before (function () {
			expect (identity).to.be.an.instanceof (Identity);
			expect (identity).to.have.property ('id');
			var promise = service.create (identity.id);

			return promise.then (function (p) {
				permanode = p;
				return when.all([
					expect (permanode).not.to.be.undefined,
					expect (permanode).not.to.be.null
				]);
			});
		});

		after (function () {
			return dao.rm (permanode);
		});

		it ('is signed', function () {
			expect (permanode).to.respondTo ('toJSON');

			expect (permanode.toJSON ()).to.contain.property ('meta');
			expect (permanode.toJSON ().meta).to.contain.property ('signatures');
			expect (permanode.toJSON ().meta.signatures).not.to.be.empty;
			var issuer = permanode.toJSON ().meta.signatures[0];

			expect (issuer.signature, 'issuer.signature').not.to.be.undefined;
			expect (issuer.issuer, 'issuer_id').not.to.be.undefined;
		});


	});
});


describe ('Claim', function () {
	var Claim = nodes.Claim,
	Permanode = nodes.Permanode,
	identity = null;

	before (function () {
		return Identity.generate ().then (function (i) {
			identity = i;
			return expect (identity).to.be.an.instanceof (Identity);
		});
	});

	describe ('given a permanode', function () {
		var permanode;

		before (function () {
			return Permanode.create (identity).then (function (i) {
				permanode = i;
				expect (permanode).to.be.an.instanceof (Permanode);
			});
		});

		it ('can be created for a single tag', function () {
			var claim = Claim.create (identity, permanode, 'set-title', 'test-title');
			return when.all([
				expect (claim).to.be.eventually.fulfilled,
				expect (claim).to.be.eventually.an.instanceof (Claim),
				expect (claim).to.eventually.have.a.property ('type', 'set-title'),
				expect (claim).to.eventually.have.a.property ('claim'),
				expect (claim).to.eventually.have.a.deep.property ('claim.value', 'test-title')
			]);
		});

		it ('can be multiple', function () {
			var claim = Claim.create (
				identity,
				{
					permanode: [
						{
							'set-title': 'test-title'
						},
						{
							'set-atttribute': {
								'test-attribute': 'test-value'
							}
						}
					]
				});

			return when.all([
				expect (claim).to.be.eventually.fulfilled,
				expect (claim).to.be.eventually.an.instanceof (Claim),
				expect (claim).to.eventually.have.a.property ('type', 'multi')
			]);
		});

	});
});


describe ('File', function () {
	var File = nodes.File,
	BlobRef = nodes.BlobRef,
	BytesRef = nodes.BytesRef,
	Chunk = nodes.Chunk,
	Claim = nodes.Claim,
	Permanode = nodes.Permanode;

	var buffer;

	var random_string = function (length) {
		var string = '', i, r,
		chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		length = length || 32;

		for (i = 0; i < length; i++) {
			r = Math.floor (Math.random () * chars.length);
			string += chars.substring (r, r + 1);
		}
		return string;
	};

	var randomStringForTest = random_string (1024);

	before (function () {
		buffer = new Buffer (randomStringForTest, 'utf8');
	});


	it ('can be created from a buffer', function () {
		expect (buffer).not.to.be.empty;

		var file = File.create.from_buffer (buffer, "test-file");

		return when.all ([
			expect (file).to.be.eventually.fulfilled,
			expect (file).to.be.eventually.an.instanceof (File),
			expect (file).to.eventually.respondTo ('toJSON')
		]);
	});

	describe ('can be created from chunks that already exist', function () {

		var chunk, blobref, bytesref;

		before (function () {
			var promise = Chunk.create (buffer);

			return when.all ([
				expect (promise).to.be.eventually.fulfilled,
				expect (promise).to.be.eventually.an.instanceof (Chunk),
				expect (promise).not.to.be.eventually.undefined,
				promise.then (function (c) {
					chunk = c;
				})
			]);
		});

		before (function () {
			var promise = BlobRef.create (chunk), p2;

			p2 = BytesRef.create ([promise]);

			return when.all ([
				expect (promise).to.be.eventually.fulfilled,
				expect (promise).to.be.eventually.an.instanceof (BlobRef),
				expect (promise.then (
					function (p) {
						return p.getLength ();
					})).to.eventually.equal (chunk.getLength ()),

				promise.then (function (p) {
					blobref = p;
				}),

				expect (p2).to.be.eventually.fulfilled,
				expect (p2).to.be.eventually.an.instanceof (BytesRef),

				p2.then (function (p) {
					bytesref = p;
				})
			]);
		});


		var file;

		before (function () {
			expect (chunk).to.be.an.instanceof (Chunk);
			var promise = File.create.from_bytesref (bytesref, "test-file");

			return when.all([
				expect (promise).to.be.eventually.fulfilled,
				expect (promise).to.be.eventually.an.instanceof (File),
				expect (promise).to.eventually.respondTo ('toJSON'),
				promise.then (function (f) {
					file = f;
					return f;
				})
			]);
		});

		it ('has the required methods', function () {
			expect (file).to.respondTo ('toJSON');
		});
	});
});
