/*jslint node: true, ass: true, white: true, nomen: true, todo: true, unparam:true, plusplus: true, vars: true */
/*jshint expr: true */
/*global describe: false, before: false, after: false, it: false */
"use strict";

require ('when/monitor/console');

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(["when", "../lib/context", "../lib/document/identity",
	"../lib/document/nodes", "chai", "chai-as-promised"], function (when, context, identity,
									nodes, chai, chaiAsPromised) {
var Chunk = nodes.Chunk;
var Identity = identity.Identity;

/* Testing framework */
var expect = chai.expect;
chai.use(chaiAsPromised);


describe ('File', function () {

	function random_string (length) {
		var string = '', i, r,
		chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
			'abcdefghijklmnopqrstuvwxyz';

		length = length || 32;

		for (i = 0; i < length; i++) {
			r = Math.floor (Math.random () * chars.length);
			string += chars.substring (r, r + 1);
		}
		return string;
	}

	var buffer = null, randomStringForTest = random_string (1024), filename = 'test-filename', xattrs = {}, fileService = null, fileDao = null, chunkDao = null;

	before (function (done) {
		context.getProto ('chunkDao', function (dao) {
			chunkDao = dao;
			expect (chunkDao).not.to.be.undefined;
			expect (chunkDao).not.to.be.null;
			done ();
		});
	});
	before (function (done) {
		context.getProto ('fileDao', function (dao) {
			fileDao = dao;
			expect (fileDao).not.to.be.undefined;
			expect (fileDao).not.to.be.null;
			done ();
		});
	});
	before (function (done) {
		context.getProto ('fileService', function (service) {
			fileService = service;
			expect (fileService).not.to.be.undefined;
			expect (fileService).not.to.be.null;
			done ();
		});
	});


	before (function () {
		buffer = new Buffer (randomStringForTest, 'utf-8');
	});

	it ('can be created from a Buffer which has no instances as a blob', function () {
		var promise, r;
		promise = fileService.uploadFile (buffer, filename, xattrs);

		r = when.all([
			expect (promise).to.be.fulfilled,
			expect (promise).not.to.eventually.be.null,
			expect (promise).to.eventually.be.an.instanceof (nodes.File),
			expect (promise).to.eventually.have.a.property ('id'),
			expect (promise).to.eventually.have.a.property ('filename', filename),
			expect (promise).to.eventually.have.a.property ('bytesref'),
			expect (promise).to.eventually.have.a.property ('xattrs')
		]);

		// At this time there must be a blob in the db
		describe ('check for existing blob', function () {
			var file, chunk;
			before(function (done) {
				promise.then (function (i) { file = i; done(); });
			});
			it ('exists in the db', function () {
				var promise = chunkDao.fetch (file.bytesref.refs[0].ref);
				return when.all([
					expect (promise).to.be.fulfilled,
					expect (promise).to.eventually.be.an.instanceof (nodes.Chunk),
					expect (promise).eventually.not.to.be.null,
					promise.then (function (blob) {
						chunk = blob;
						return blob;
					}, function (e) {
						expect(null).to.be.false;
					})
				]);
			});

			it ('has bytes with the required length', function () {
				var promise = chunkDao.fetch (file.bytesref.refs[0].ref);
				return when.all([
					expect (promise).to.be.fulfilled,
					expect (promise).not.to.be.eventually.null,
					expect (promise).to.eventually.respondTo ('getLength'),
					expect (promise).to.eventually.satisfy (function (chunk) {
						return chunk.getLength() == chunk.content.length;
					}),
					expect (promise).to.eventually.satisfy (function (chunk) {
						return chunk.getLength() == buffer.length;
					})
				]);
			});

			after (function () {
				return chunkDao.delete (chunk);
			});
		});

		after (function () {
			return promise.then (function (file) {
				return fileDao
					.delete (file) // I'm nesting
						       // the promises
						       // here to
						       // avoid
						       // encapsulating
						       // the field I
						       // want as an
						       // array
				.then (function () {
					return file;
				});
			}).then (function (file) {
				return chunkDao.fetch (file.bytesref.refs[0].blobRef);
			}).then (function (blob) {
				return chunkDao.delete (blob);
			});
		});

		return r;
	});

	describe ('given an old published version', function () {
		// We must create 2 bytesrefs
		var secondRandomString = random_string (100),
		buffer2 = buffer + new Buffer (secondRandomString, 'utf-8'),
		file, promise;

		// First create a file
		before (function () {
			promise = fileService.uploadFile (buffer, filename, xattrs);

			expect (promise).to.be.fulfilled;
			expect (promise).to.eventually.be.an.instanceof (nodes.File);

			return promise.then (function (f) {
				file = f;
				return f;
			});
		});

		var blobService;

		before (function (done) {
			context.getProto ('blobService', function (service) {
				blobService = service;
				expect (blobService).not.to.be.undefined;
				expect (blobService).not.to.be.null;
				done ();
			});
		});


		it ('has a database id', function () {
			expect (file._couchdb_id).not.to.be.undefined;
			expect (file._couchdb_id).not.to.be.null;
		});


		it ('its refs respond to getContent', function () {
			var ref = file.bytesref.refs[0];
 			var p = ref.getContent (blobService);

			return when.all ([
				expect (p).to.be.fulfilled,
				expect (p).to.be.eventually.an.instanceof (Buffer),
				expect (p).to.eventually.satisfy (function (p) {
					return p.toString () === buffer.toString ();
				})
			]);
		});


		it ('can be partially updated', function () {
			var changeset = [
				{ type: '+', value: '<<<inserted>>>', pos: 4 }
			];

			promise = fileService.partialUpload (file.id, changeset);
			after (function () {
				return promise.then (function (file) {
					return when.all([
						expect (file).not.to.be.undefined,
						expect (file).not.to.be.null,
						fileDao.delete (file),
						chunkDao.fetch (
							file.bytesref.refs[0].blobRef
						).then (function (blob) {
							return chunkDao.delete (blob);
						})
					]);
				});
			});

			// It should probably have two nodes in its bytesref
			var p_bytesref = promise.then (function (file) {
				return file.bytesref;
			});

			return when.all([

				expect (promise).to.be.fulfilled,
				expect (promise).to.eventually.be.an.instanceof (nodes.File),

				expect (p_bytesref).to.be.fulfilled,
				expect (p_bytesref).not.to.be.eventually.null,
				expect (p_bytesref).to.eventually.be.an.instanceof (nodes.BytesRef),
				expect (p_bytesref).to.eventually.have.property ('refs'),
				expect (p_bytesref.then (function (bytesref) {
					return bytesref.refs;
				})).to.eventually.have.length.within (2, 5),
				expect (p_bytesref.then (function (bytesref) {
					return bytesref.refs;
				})).to.eventually.satisfy (function (refs) {
					return refs.filter (function (ref) {
						expect (ref.ref).not.to.be.undefined;
						expect (ref.ref).not.to.be.null;
						expect (ref.refType).not.to.be.undefined;
						expect (ref.refType).not.to.be.null;
						expect (ref.size).not.to.be.undefined;
						expect (ref.size).not.to.be.null;
						return true;
					}).length == refs.length;
				})
			]);
		});

	  after (function () {
			expect (file).not.to.be.undefined;
			expect (file).not.to.be.null;
			return when.all([
				fileDao.delete (file),

				chunkDao.fetch (file.bytesref.refs[0].blobRef)
					.then (function (blob) {
						return chunkDao.delete (blob);
					})
			]);
		});
	});

	describe ('a multiple-chunk file', function () {
		var files = [], promise;
		var secondRandomString = random_string (100),
		thirdRand = random_string (20);


		before (function () {
			promise = fileService.uploadFile (buffer, filename, xattrs);
			return promise.then (function (file) {
				return files.push (file);
			});
		});

		before (function () {
			var changeset = [{
				'type': '+',
				'value': secondRandomString,
				'pos': 4
			}, {
				'type': '+',
				'value': thirdRand,
				'pos': 80
			}];

			promise = fileService.partialUpload (files[0].id, changeset);

			return when.all ([
				expect (promise).to.be.fulfilled,
				promise.then (function (i) {
					files.push (i);
				})
			]);
		});

		it ('can obtain its length', function () {
			var file = files [1];

			return when.all ([
				expect (file).not.to.be.undefined,
				expect (file).not.to.be.null,
				expect (file).to.be.an.instanceof (nodes.File),

				expect (fileService).to.respondTo ('getLength'),
				expect (fileService.getLength (file.id)).to.be.fulfilled,
				expect (fileService.getLength (file.id)).to.eventually.equal (100 + 20 + 1024),
				expect (file.bytesref.refs).to.satisfy (function (refs) {
					return refs.every (function (ref) {
						return ref.getLength () > 0;
					});
				}, 'all bytesref must have positive lengths')
			]);

		});

		it ('can obtain its content', function () {

			var file = files [1];

			var strings = function (promise) {
				return promise.then (function (entity) {
					return entity.toString ();
				});
			};

			var firstTwoRefs = new Buffer (4 + 100);

			buffer.copy (firstTwoRefs, 0, 0, 4);
			(new Buffer (secondRandomString)).copy (firstTwoRefs, 4, 0, 100);

			return when.all ([
				expect (file).not.to.be.undefined,
				expect (file).not.to.be.null,
				expect (file).to.be.an.instanceof (nodes.File),

				expect (fileService).to.respondTo ('getLength'),
				expect (fileService).to.respondTo ('getBytes'),

				expect (strings (fileService.getBytes (file.id, 0, 4))).to.eventually.equal (buffer.slice (0, 4).toString (), 'first chunk'),
				expect (strings (fileService.getBytes (file.id, 4, 104))).to.eventually.equal (secondRandomString, 'second chunk'),
				expect (strings (fileService.getBytes (file.id, 180, 200))).to.eventually.equal (thirdRand, 'third chunk'),
				expect (strings (fileService.getBytes (file.id, 0, 104))).to.eventually.equal (firstTwoRefs.toString (), 'first and second chunk together'),
				expect (strings (fileService.getBytes (file.id, 4, 100))).to.eventually.equal (secondRandomString.slice (0, 96), 'second chunk up to 96 bytes'),
				expect (strings (fileService.getBytes (file.id, 5, 14))).to.eventually.equal (secondRandomString.slice (1, 10), 'partially second chunk')
			]);

		});

		after (function () {
			var q = [], file;

			var delete_blob = function (blob) {
				return chunkDao.delete (blob);
			};

			while ((file = files.pop())) {
				expect (file).not.to.be.undefined;
				expect (file).not.to.be.null;
				q.push (fileDao.delete (file));
				q.push (chunkDao.fetch (file.bytesref.refs[0].blobRef)
					.then (delete_blob));
			}
			return when.all (q);
		});
	});
});

});
