var assert = require('assert'),
    context = require('../lib/context'),
    events = require('events'),
    nodes = require('../lib/document/nodes'),
    chunk = nodes.Chunk,
    Identity = require('../lib/document/identity').Identity;

var chai = require("chai");
var when = require("when");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var expect = chai.expect;


describe('New Identity', function () {

	it ('can be generated', function () {

		var promise = Identity.generate();

		return when.all([
			expect (promise).to.be.eventually.fulfilled,
			expect (promise).to.be.eventually.an.instanceof (Identity),
			expect (promise).to.eventually.respondTo ('toJSON')
		]);
	});
});


describe ('Identity API', function () {
	it ('has the proper instance methods', function () {
		expect (Identity).to.respondTo ('toJSON');
		expect (Identity).to.respondTo ('sign');
		expect (Identity).to.respondTo ('verify');
		expect (Identity).itself.to.respondTo ('verifyAll');
		expect (Identity).itself.to.respondTo ('generate');
	});
});


describe ('An instance of identity', function () {
	var identity, dao, storage;

	before (function () {
		return Identity.generate().then (function (i) {
			identity = i;
		});
	});

	before (function (done) {
		return context.getProto ('identityDao', function (i) {
			dao = i;
			done();
		});
	});

	before (function (done) {
		return context.getProto ('nodeStorage', function (i) {
			storage = i;
			identity.id = storage.getIdForEntity (identity);
			done();
		});
	});

	before (function () {
		expect (identity, 'identity').not.to.be.undefined;
		expect (dao, 'dao').not.to.be.undefined;
		expect (storage, 'storage').not.to.be.undefined;
	});


	it ('can be saved onto the database', function () {

		var promise = dao.save (identity);

		return when.all ([
			expect (promise).to.be.eventually.fulfilled,
			expect (promise).to.be.eventually.an.instanceof (Identity),
			expect (promise).to.eventually.contain.key ('id')
		]);
	});

	it ('can be recovered from database', function () {
		expect (identity.id).not.to.be.null;

		var promise = dao.fetch (identity.id);

		return when.all ([
			expect (promise).to.be.fulfilled,
			expect (promise).to.be.eventually.an.instanceof (Identity),
			expect (promise).to.eventually.contain.property ('id', identity.id),
			expect (promise).to.eventually.contain.property ('pubKey', identity.pubKey),
			expect (promise).to.eventually.contain.property ('privKey', identity.privKey)
		]);
	});

  describe ('may sign a document', function () {
    var claim, cdao;

    before (function (done) {
      context.getProto ('claimDao', function (d) {
	cdao = d;
	done();
      });
    });

    before (function () {
      claim = nodes.Claim.create (identity,
                                  "SHA!PERMANODE",
                                  'test-claim',
                                  { attribute: "hello", value: "world" }
                                 );

      return claim.then (cdao.save.bind(cdao)).then(function (c) { claim = c; });
    });

    after (function () {
      cdao.delete (claim);
    });

    it ('can sign it', function () {
      return identity.sign (claim.toJSON()).then (function (signature) {
               claim.signatures.push (signature);
               expect (claim).to.contain.property('signatures');
               expect (claim).to.have.property('signatures').with.length.above(1);
             });
    });
    it ('can save the new document', function () {
      return cdao.save (claim).then (function (x) {
               return cdao.fetch (claim._couchdb_id).then (function (claim) {
                        expect (claim).to.have.property('signatures').with.length.above(1);
                      });
             });
    });

  });

	after (function () {
		return dao.delete (identity);
	});

});
