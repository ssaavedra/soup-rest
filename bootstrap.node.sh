#!/bin/bash

ip addr
export KADOH_TRANSPORT=udp
export BOOTSTRAPS=${BOOTSTRAPS:-'["172.16.0.1:3000"]'}
export DHTPORT=5000
NODE_CONFIG=$(echo '{"dht":{"node":{' \
	'"bootstraps": ' $BOOTSTRAPS ', ' \
	'"port": ' $DHTPORT '}}}')

export NODE_CONFIG

echo NODE_CONFIG = $NODE_CONFIG
node lib/server/main.js
exit

# Execute our stuff automatically
export KADOH_TRANSPORT=udp
which nodemon || sudo npm install -g nodemon
rm -f ~/node.log
touch ~/node.log
killall node

# Make a bit interactive the output
nodemon -I -L lib/server/main.js >~/node.log  2>&1 &

tail -f ~/node.log &
TAILPID=$!
sleep 10
kill $TAILPID

