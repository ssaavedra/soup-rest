/*jslint node: true, white: true, nomen: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      crypto = require('crypto'),
      couchdbdb = require('../couchdb-db'),
      stringify = require('json-stable-stringify'),
      when = require('when');

  var dgst_alg = 'sha1';

  var CouchDBStorage = function (dbname) {
    this.db = couchdbdb.db(dbname);
    this.db.stopCaching ();
  };

  CouchDBStorage.prototype.store = function (id, doc) {

    if (doc === undefined) {
      doc = id;
      id = this.getIdForDocument (doc);
    }

    if (!doc.content) {
      var c = {
	_id : id,
	content: doc
      };

      doc = c;
    } else {
      doc._id = id;
    }

    return this.db.save(doc);
  };

  CouchDBStorage.EntityNotFound = function () {
    Error.apply (this, arguments);
  };

  CouchDBStorage.EntityNotFound.prototype = Object.create (Error.prototype);


  CouchDBStorage.prototype.fetch = function (id) {
    //	return this.db.get (id);

    return this.db.get (id).then (function (doc) {
	     if (doc === null) {
	       throw new CouchDBStorage.EntityNotFound ("Entity " + id + " not found");
	     }
	     return doc;
	   });
  };

  CouchDBStorage.prototype.remove = function (id, rev) {
    return this
           .db
           .request({ method: 'DELETE',
                      path: '/' + encodeURIComponent(id) + '?rev=' + rev})
           .catch (function (e) {
	             if (e.error == "not_found") {
                       // Already was not there
	             } else throw e;
	           });
  };

  CouchDBStorage.prototype.getSerialization = function (doc) {
    return stringify (doc);
  };

  CouchDBStorage.prototype.getIdForDocument = function (doc) {
    var dgst = crypto.createHash(dgst_alg);

    dgst.update(this.getSerialization(doc.content));

    return dgst_alg + '-' + dgst.digest('hex');
  };

  CouchDBStorage.prototype.getIdForEntity = function (entity) {
    return this.getIdForDocument (entity.toJSON());
  };

  CouchDBStorage.prototype.view = function (viewname, params) {
    var view = this.db.database + "/" + viewname;
    return this.db.view (view, params);
  };


  var MockDBStorage = function (dbname) {
    this.dbname = dbname;
    this._database = {};
  };

  MockDBStorage.prototype.copy = function (obj) {
    return JSON.parse(JSON.stringify(obj));
  };

  MockDBStorage.prototype.store = function (id, doc) {

    if (doc === undefined) {
      doc = id;
      id = this.getIdForDocument (doc);
    }

    if (!doc.content) {
      var c = {
	_id : id,
	content: doc
      };

      doc = c;
    } else {
      doc._id = id;
    }

    if (doc._id in this._database) {
      if (doc._rev == this._database[doc._id]._rev) {
	// Update doc
	this._database[doc._id] = this.copy(doc);
	this._database[doc._id]._rev++;
      } else {
	return when.reject("ENTITY ALREADY THERE; NO OR BAD REV. Given REV=" + doc._rev);
      }
    } else {
      this._database[doc._id] = this.copy(doc);
      this._database[doc._id]._rev = 1;
    }

    return when(this.copy(this._database[doc._id]));
  };

  MockDBStorage.prototype.fetch = function (id) {

    if (id in this._database) {
      return when (this._database[id]);
    } else {
      return when.reject (null);
    }
  };

  MockDBStorage.prototype.getSerialization = function (doc) {
    return stringify (doc);
  };

  MockDBStorage.prototype.getIdForDocument = function (doc) {
    var dgst = crypto.createHash(dgst_alg);

    dgst.update(this.getSerialization(doc.content));

    return dgst_alg + '-' + dgst.digest('hex');
  };

  MockDBStorage.prototype.getIdForEntity = function (entity) {
    return this.getIdForDocument (entity.toJSON());
  };

  MockDBStorage.prototype.remove = function (id, rev) {
    if (id === undefined) {
      console.trace (new Error("trying to remove undefined"));
    }

    if (id in this._database) {
      if (this._database[id]._rev == rev) {
	return when (true);
      } else {
	return when.reject (false);
      }
    }
    return when.reject (false);
  };

  // return MockDBStorage
  return CouchDBStorage;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
