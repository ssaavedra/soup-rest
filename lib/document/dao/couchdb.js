/*jslint node: true, white: true, nomen: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {
  var assert = require('assert'),
      when = require('when'),
      log = require('../../log'),
      DaoInterface = require('./interface'),
      nodes = require('../nodes');

  var logger = log.getLogger ('Storage.DaoCouchDB');

  var DaoCouchDB = function DaoCouchDB(deps) {
    logger.trace ("INSTANTIATING DEPS = ", deps);
    this.db = deps.db;
    this.dht = deps.dht;
  };

  DaoCouchDB.prototype = Object.create (DaoInterface.prototype);
  DaoCouchDB.prototype._getId = function (entity) {
    return this.db.getIdForEntity (entity);
  };

  DaoCouchDB.prototype._save = function (id, entity) {
    var doc = entity.toJSON();
    if (!doc._id) {
      doc._id = id;
    }

    if (!doc._id) {
      logger.error ("Trying to persist an entity without an ID! (%s)", doc);
      throw new Error("Trying to persist id-less entity");
    } else {
      logger.debug ("Persisting document with id %s and type %s", doc._id, doc.content.type);
    }

    // logger.info ("Saving entity ", doc.content.type, " id = ", doc._id);

    // Test first if entity already exists, and if it does, noop.

    return this.db.fetch (id).catch (function () {
             logger.trace ("Document about to persist does not exist on DB yet");
             return { _id: doc._id };
           }).then (function (xdoc) {
                      doc._rev = xdoc._rev;
                      doc._id = xdoc._id;
                      logger.trace ("Storing doc with id = ", doc._id, " and rev = ", doc._rev);
                      return this.db.store (doc);
           }.bind(this)).then (function (doc) {
             entity._couchdb_id = doc._id;
             entity._couchdb_rev = doc._rev;
             logger.trace ("Stored new document, rev is ", doc._rev);
             return entity;
           });

    return (this
            ._fetch (id)
            .catch(function () {
	             return this.db
		            .store (doc)
		            .then(function (doc) {
			      entity._couchdb_id = doc._id;
			      entity._couchdb_rev = doc._rev;
			      // console.log ("Set couchdbid to doc id", entity._couchdb_id, doc._id);
			      return entity;
		            }, function (d) {
			         logger.debug ("Could not save entity " + entity.toJSON ().content.type);
			         logger.debug ("... due to " + d + ".");
			       });
	           }.bind(this))
            .tap
            (function (entity) {
              this.dht
              .set (entity._id, entity)
              .catch (function () {
		        logger.info ("DHT not initialized");
	              });
            }.bind (this)));
  };

  DaoCouchDB.prototype._fetch = function (id) {
    var doc_p = this.db.fetch (id), entity_p, couchdb_id, couchdb_rev;

    entity_p = doc_p.then (function (doc) {
		 couchdb_id = doc._id;
		 couchdb_rev = doc._rev;
		 return doc;
	       }).then (this._instantiate).then (function (entity) {
		 entity._couchdb_id = couchdb_id;
		 entity._couchdb_rev = couchdb_rev;

		 return entity;
	       });

    return when (entity_p);
  };

  DaoCouchDB.prototype._delete = function (entity) {
    var id = entity._couchdb_id, rev = entity._couchdb_rev;

    // console.warn ("[DAO] Deleting entity ID ", id);
    if (id === undefined || rev === undefined) {
      logger.trace (new Error("trying to remove undefined" + id + rev));
      throw new Error ("Trying to remove undefined " + entity.toJSON().content.type);
      return false;
    }

    assert.ok (id && rev);

    return this.db.remove (id, rev);
  };


  return DaoCouchDB;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
