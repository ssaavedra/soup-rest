/*jslint node: true, white: true, nomen: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {
  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes');

  var Chunk = nodes.Chunk;

  var ChunkDao = function ChunkDao (deps) {
    DaoCouchDB.call (this, deps);

    this._instantiate = function (doc) {
      var buffer = new Buffer (doc.content.content, 'base64'), chunk;

      if (doc.content.type != "blob") {
	console.log ("Content was not blob!", doc);
	throw new Error(doc.content);
      }

      chunk = new Chunk (doc._id, buffer);

      return chunk;
    };

  };

  ChunkDao.prototype = Object.create (DaoCouchDB.prototype);

  return ChunkDao;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
