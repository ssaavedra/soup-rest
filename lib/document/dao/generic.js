/*jslint node: true, white: true, nomen: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {
  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes');

  var Generic = nodes.GenericEntity;

  var GenericDao = function GenericDao (deps) {
    DaoCouchDB.call (this, deps);

    this._instantiate = function (doc) {
      var entity = new Generic (doc);

      return entity;
    };

  };

  GenericDao.prototype = Object.create (DaoCouchDB.prototype);

  return GenericDao;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
