/*jslint node: true, ass: true, white: true, nomen: true, plusplus: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes');

  var Permanode = nodes.Permanode;

  var PermanodeDao = function PermanodeDao (deps) {
    DaoCouchDB.call (this, deps);

    this._instantiate = function (doc) {
      var permanode = new Permanode (doc._id, doc.content, doc.meta.signatures);

      return permanode;
    };

};

  PermanodeDao.prototype = Object.create (DaoCouchDB.prototype);

  return PermanodeDao;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
