
(function (define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes'),
      identity = require('../identity');

  var Identity = identity.Identity;

  var IdentityDao = function IdentityDao (deps) {
    DaoCouchDB.call (this, deps);

    this._instantiate = function (doc) {
      var identity = new Identity (doc._id, doc.content.name, doc.content.pubKey, doc.content.privKey);
      return identity;
    };

  };

  IdentityDao.prototype = Object.create (DaoCouchDB.prototype);

  IdentityDao.prototype.getAll = function () {
    var docs = this.db.view ('id_all'), _instantiate = this._instantiate.bind (this);

    return docs.then (function (docs) {
      return docs.rows
	.map (function (doc) { return doc.value; })
	.map (_instantiate);
    });
  };

  IdentityDao.prototype.getSignable = function () {
    var docs = this.db.view ('id_signable'), _instantiate = this._instantiate.bind (this);

    return docs.then (function (docs) {
      return docs.rows
	.map (function (doc) { return doc.value; })
	.map (_instantiate);
    });
  };

  return IdentityDao;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
