/*jslint node: true, ass: true, white: true, nomen: true, plusplus: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var DaoInterface = function DaoInterface () {
    return;
  };

  DaoInterface.prototype.save = function (entity) {
    var id = this._getId (entity);
    return this._save (id, entity);
  };

  DaoInterface.prototype.fetch = function (id) {
    return this._fetch (id);
  };

  DaoInterface.prototype.rm = function (entity) {
    return this._delete (entity);
  };

  DaoInterface.prototype.delete = DaoInterface.prototype.rm;


  module.exports = DaoInterface;

  return DaoInterface;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
