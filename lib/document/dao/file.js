/*jslint node: true, ass: true, white: true, nomen: true, plusplus: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes');

  var BytesRef = nodes.BytesRef,
      File = nodes.File;

  var FileDao = function FileDao (deps) {
    DaoCouchDB.call (this, deps);
    this._instantiate = function (doc) {
      var bytesref = BytesRef.create_from_json (doc.content.bytesRef);
      var file = new File (doc.content.filename, bytesref, doc.content.xattrs);
      file.id = doc._id;
      return file;
    };

  };

  FileDao.prototype = Object.create (DaoCouchDB.prototype);

  FileDao.prototype.isFile = function (identifier) {
    return this.fetch (identifier).then(function (doc) {
	     return doc.type === "file";
	   });
  };


  return FileDao;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
