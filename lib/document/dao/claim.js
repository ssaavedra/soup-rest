/*jslint node: true, white: true, nomen: true, plusplus: true, vars: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      DaoInterface = require('./interface'),
      DaoCouchDB = require('./couchdb'),
      nodes = require('../nodes'),
      when = require('when');


  var ClaimDao = function ClaimDao (storage) {
    DaoCouchDB.call (this, storage);

    var instantiate_single = function (doc) {
      var base;

      base = new nodes.SubClaim (doc.content.permanode,
                                 doc.content.issuer,
                                 doc.content.claimType,
                                 doc.content.sub,
                                 new Date (doc.content.date),
                                 doc._id);

      return new nodes.Claim (base, doc.meta.signatures, base);
    };

    var subinstatiate_multi = function (doc, issuer) {
      var bases = [], i, cur, cur_sub;


      for (i = 0; i < doc.sub.length; i++) {
        cur = doc.sub[i];

        if (cur.claimType === 'multi') {
          cur_sub = subinstatiate_multi (cur, issuer);
        } else {
          cur_sub = new nodes.SubClaim (cur.permanode,
                                        issuer,
                                        cur.claimType,
                                        cur.sub);
        }

        bases.push (cur_sub);
                }

      return new nodes.ManyClaim (issuer, new Date (doc.content.date), bases);
    };

    var instantiate_multi = function (doc) {
      var base = subinstatiate_multi (doc.content, doc.content.issuer);

      return new nodes.Claim (base, doc.meta.signatures);
    };

    this._instantiate = function (doc) {
      // Depending on how many permanodes the claim is about
      // we have to instantiate it differently

      if (doc.claimType === 'multi') {
        return instantiate_multi (doc);
      }
      return instantiate_single (doc);
    };

  };

  ClaimDao.prototype = Object.create (DaoCouchDB.prototype);

  var __instantiate = function (instantiate_function) {
    return function (row) {
      return instantiate_function (row.doc);
    };
  };

  ClaimDao.prototype.fetchByPermanode = function (permanode, attribute) {
    var startkey = ["permanode", permanode],
        endkey = ["permanode", permanode],
        _instantiate = this._instantiate,
        group_level = 2;

    if (attribute) {
      startkey.push (attribute);
      endkey.push (attribute);
      group_level = 3;
    }
    endkey.push ("z");

    return this.db.view ('claims_ext',
                         { startkey: startkey,
                           endkey: endkey,
                           reduce: true,
                           group: true,
                           group_level: group_level
                         })
           .then(function (docs) {

             if (docs.rows.length === 0) {
               return [];
             }

             return docs.rows[0].value.cur.map (_instantiate);
           });
  };

  ClaimDao.prototype.fetchAllByPermanode = function (permanode) {
    var docs = this.db.view ('claims', { key: permanode }),
        _instantiate = (this._instantiate);

    return docs.then(function (docs) {
             return docs.rows.map (function (doc) {
                      return doc.value;
                    }).map(_instantiate);
           });
  };

  ClaimDao.prototype.findByName = function (query) {
    var docs = this.db.view ('by_name', { key: query,
                                          include_docs: true }),
        _instantiate = __instantiate (this._instantiate);

    return docs.then (function (docs) {
             return docs.rows.map (_instantiate);
           });
  };

  ClaimDao.prototype.findByCustomAttribute = function (key, value) {

    /* First we look up claims by attribute name and value */
    var first_search = this.db.view ('claims_ext',
                                     {
                                       startkey: ["attribute", key, value ],
                                       endkey: ["attribute", key, value, "z"],
                                       reduce: true,
                                       group: true,
                                       group_level: 3
                                     });

    /* Now we get the current claims out of there */
    var possible_claims = first_search.then (function (docs) {
                                               if (docs.rows.length > 0) {
                                                 return docs.rows[0].value.cur;
                                               }
                                               return [];
                                             }.bind(this));

    var test_claim = function (claim) {
      var permanode = claim.content.permanode,
          claim_id = claim._id,
          attribute = claim.content.sub.attribute;

      /* And as the "attribute" key claims are not properly reduced,
       * we test against them in the "permanode" keys,which are
       * properly reduced so that we can check if there was a later
       * del-attribute which may invalidate our current claim.
       */
      return this
             .db
             .view ('claims_ext', { startkey: ["permanode", permanode, attribute],
                                    endkey: ["permanode", permanode, attribute, "z"],
                                    reduce: true,
                                    group: true,
                                    group_level: 3
                                  })
             .then(function (results) {

               /* Return: bool: true if the value is already in the
                * past of the permanode, i.e., the permanode has a
                * later "set-attribute" or a "del-attribute" which is
                * valid
                */
               return (results
                       .rows[0].value.past
                       .some (function (past) {
                         return past._id === claim_id;
                       }));
             });
    }.bind (this);

    var vigent_claims = possible_claims.then(function (claims) {
                          return when.all(claims.map (function (claim) {
                                   return when.all ([claim, test_claim (claim)]);
                                 }));
                        });

    vigent_claims = vigent_claims.then (function (claims) {
                      return claims.filter (function (cc) {
                               var claim = cc[0],
                                   test = cc[1];
                               return !test;
                             }).map (function (cc) {
                               return cc[0];
                             });
                    });

    return when.map (vigent_claims, this._instantiate);
  };

  return ClaimDao;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
