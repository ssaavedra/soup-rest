/*jslint node: true, ass: true, white: true, nomen: true, todo: true, unparam:true, plusplus: true, vars: true */
/*jshint expr: true */
/*global describe: false, before: false, after: false, it: false */
"use strict";

(function (define) {
define(function (require, exports, module) {
  var assert = require('assert'),
      when = require('when'),
      context = require('../context'),
      db = require('../couchdb-db'),
      e = require('events'),
      Identity =  require('./identity');


  /** Utility to inject the ID for an entity. Injects entity.id,
   * returning entity again.
   * @param {NodeHolder} entity
   * @param {Object} r - returned object in the promise (defaults to the entity itself)
   * @return Promise(NodeHolder) entity itself
   */
  var injectId = function getIdForDocument (entity, r) {
    var w = when.defer ();

    context.getProto ('nodeStorage', function (storage) {
      entity.id = storage.getIdForDocument (entity.toJSON ());
      w.resolve (r || entity);
    });

    return w.promise;
  };


  /** @class */
  var NodeHolder = exports.NodeHolder = function () { return undefined; };

  NodeHolder.prototype.toJSON = function () {
    return {
      "content": {
	"type": "stub"
      }
    };
  };

  /**
   * @constructor
   * 
   * @param {{}} The inner JSON document
   */
  var GenericEntity = exports.GenericEntity = function GenericEntity (wrapped_doc) {
    this.doc = wrapped_doc;
  };

  GenericEntity.prototype.toJSON = function () {
    return this.doc;
  };


  /**
   * @class
   * @constructor
   *
   * @param {NodeHolder} wrapped_entity
   * @param {signature_array} signatures
   */
  var SignedNode = exports.SignedNode = function SignedNode (wrapped_entity, signatures) {
    NodeHolder.call (this);

    this.entity = wrapped_entity;
    this.signatures = signatures;
  };


  SignedNode.create = function (entity, identity) {
    var signature = identity.sign (entity.toJSON());

    return signature.then (function (signature) {
	     return new SignedNode (entity, [signature]);
	   });
  };

  SignedNode.prototype = Object.create (NodeHolder.prototype);

  SignedNode.prototype.toJSON = function () {
    var doc = this.entity.toJSON ();

    if (!doc.meta) {
      doc.meta = {};
    }

    if (!doc.meta.signatures) {
      doc.meta.signatures = [];
    }

    doc.meta.signatures.push.apply (doc.meta.signatures, this.signatures);

    return doc;
  };


  var Chunk = exports.Chunk = function (id, buffer)
  {
    this.id = id;
    this.content = buffer;
  };

  Chunk.create = function (buffer) {
    var w = when.defer ();

    context.getProto ('nodeStorage', function (storage) {
      var ref = new Chunk (null, buffer);

      ref.id = storage.getIdForDocument (ref.toJSON ());
      w.resolve (ref);
    });

    return w.promise;
  };

  Chunk.prototype = Object.create (NodeHolder.prototype);

  Chunk.prototype.toJSON = function () {
    return {
      "content": {
	"type": "blob",
	"content": this.content.toString ('base64')
      }
    };
  };

  Chunk.prototype.getLength = function () {
    return this.content.length;
  };

  Chunk.prototype.toBlobRef = function (size) {
    return { "blobRef": this.id, "size": size || this.getLength () };
  };


  var BlobRef = exports.BlobRef = function (refType, ref, size)
  {

    if (ref instanceof Chunk) {
      this.ref = ref.id;
      this.refType = "blobRef";

      if (size === undefined) {
	size = ref.getLength();
      }
    } else {
      this.ref = ref;
      this.refType = refType;
    }

    this.size = size;
  };


  BlobRef.create = function (obj, bytes) {
    return when (obj).then (function (obj) {
	     var refType = (obj instanceof Chunk) ? 'blobRef' : 'bytesRef';
	     return new BlobRef (refType, obj, bytes);
	   });
  };

  /**
   * Creates a BlobRef from a json description such as:
   *
   * <code>
   * [ { blobRef: "sha1-blobid", size: "100" },
   *   { bytesRef: "sha-bytesref", size: "200" } ]
   * </code>
   *
   * @param {Array<Object>} json
   */
  BlobRef.create_from_json = function (json) {
    if (typeof json.toBlobRef !== "undefined") {
      json = json.toBlobRef ();
    }

    var type = json.hasOwnProperty ('blobRef') ? 'blobRef' : 'bytesRef',
	size = json.size,
	value = json.hasOwnProperty ('blobRef') ? json.blobRef : json.bytesRef;

    return new BlobRef (type, value, size);
  };


  BlobRef.prototype.getLength = function () {
    return this.size;
  };

  BlobRef.prototype.toJSON = function () {
    var o = {};
    if (this.refType) {
      o[this.refType] = this.ref;
    }

    o.size = this.size;

    return o;
  };

  // BlobRef.prototype.getContent = function (blobService) {
  var $getContent = function (blobService)  {
    var size = this.getLength ();

    if (this.refType === 'blobRef') {

      return blobService.getBlob (this.ref).then (function (blob) {
	       return blob.content.slice (0, size);
	     });

    } else if (this.refType === 'bytesRef') {

      return blobService
             .getBytesRefById (this.ref)
             .then (function (refs) {
	       var q = refs.refs.push (function (ref) {
			 return ref.getContent (blobService);
		       });

	       return when.all (q);
	     }).then (function (values) {
	       var i = 0, io = 0;
	       var buffer = new Buffer (size);

	       for (i = 0; i < values.length; i++) {
		 values[i].copy (buffer, io);
		 io += values[i].length;
	       }
	       return buffer;
	     });
    } else {
      throw new Error ("What the fuck is reftype = " + this.refType);
    }
  };

  BlobRef.prototype.getContent = function (svc) {
    var r = $getContent.call (this, svc);
    if (r === undefined) {
      throw new Error ("Content is undefined");
    }
    return r;
  };


  /**
   * A BytesRef is a reference to a bunch of BlobRef refs.
   *
   * @param {Array.<BlobRef>} blobrefs - Array of BlobRefs to encapsulate
   */
  var BytesRef = exports.BytesRef = function (blobrefs)
  {
    this.refs = blobrefs;
  };

  BytesRef.create = function (blobrefs) {
    return when
	   .all (blobrefs)
	   .then (function (blobrefs) {
	     return new BytesRef (blobrefs);
	   });
  };

  BytesRef.create_from_json = function (json) {
    return new BytesRef (json.map (BlobRef.create_from_json));
  };

  BytesRef.prototype = Object.create (NodeHolder.prototype);

  BytesRef.prototype.toJSON = function () {
    return { "content": {
      "type": "bytes",
      "parts": this.refs.map (function (ref) { return ref.toJSON (); })
    }};
  };

  BytesRef.prototype.getLength = function () {
    return this.refs.reduce (function (prev, cur, i, all) {
             return prev + cur.getLength ();
           }, 0);
  };

  /**
   * Find the position belonging to the beginning of the node at the
   * position you ask
   */
  BytesRef.prototype.refStartFromPosition = function (from) {
    var i = 0, sum = 0, last_length;
    while (sum < from) {
      last_length = this.refs[i++].getLength ();
      sum += last_length;
    }

    // Undo last step if we stepped over
    if (last_length > from) {
      sum -= last_length;
      i--;
    }
    return sum;
  };

  /**
   * Return a new BytesRef which will contain only refs in the specified from-to interval.
   */
  BytesRef.prototype.withinRange = function (from, to, exclusive) {
    var useful = [], i = 0, sum = 0, last_length;
    while (sum < from) {
      last_length = this.refs[i++].getLength ();
      sum += last_length;
    }

    // Undo last step if we stepped over
    if (last_length > from && !exclusive) {
      sum -= last_length;
      i--;
    }

    while (sum < to) {
      if (this.refs[i] === undefined) {
	break;
      }
      useful.push (this.refs[i]);
      sum += this.refs[i++].getLength ();

    }

    return new BytesRef (useful);
  };

  BytesRef.prototype.forEach = function () {
    return this.refs.forEach.apply (this.refs, arguments);
  };

  BytesRef.prototype.slice = function () {
    return new BytesRef (this.refs.slice.apply (this.refs, arguments));
  };



  var random_string = function (length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
	string = '', i = 0, r;

    length = length || 32;

    for (i = 0; i < length; i++) {
      r = Math.floor (Math.random () * chars.length);
      string += chars.substring (r, r + 1);
    }
    return string;
  };


  var UnsignedPermanode = exports.UnsignedPermanode = function (issuer_id, random)
  {
    this.id = null;
    this.doc = {
      "content": {
	"type": "permanode",
	"issuer": issuer_id,
	"random": random
      }
    };
  };

  UnsignedPermanode.create = function (creator) {
    return new UnsignedPermanode (creator.id, random_string (256));
  };

  UnsignedPermanode.prototype = Object.create(NodeHolder.prototype);
  UnsignedPermanode.prototype.toJSON = function () {
    return this.doc;
  };


  var Permanode = exports.Permanode = function (id, content, signatures)
  {
    var unsigned = new UnsignedPermanode (content.issuer, content.random);

    this.id = id;
    SignedNode.call (this, unsigned, signatures);

    // TODO: If we have a lot of time, assert.ok (creator.validate (doc))
  };

  Permanode.create = function (creator) {
    var base = UnsignedPermanode.create (creator);

    return injectId (base).then (function (b) {
	     base = b;
	     return creator.sign (base.toJSON());
	   }).then (function (signature) {
	     return new Permanode (base.id, base.toJSON().content, [signature]);
	   });
  };

  Permanode.prototype = Object.create (SignedNode.prototype);
  Permanode.prototype.constructor = Permanode;


  /**
   * @class
   * @constructor
   */
  var SubClaim = exports.SubClaim = function (permanode, issuer, type, value, date, id)
  {
    NodeHolder.call (this);

    this.id = id;
    this.issuer = issuer;
    this.type = type;
    this.permanode = permanode;
    this.permanodes = [ permanode ];
    this.value = value;
    this.date = date;
  };


  SubClaim.prototype = Object.create (NodeHolder.prototype);

  SubClaim.prototype.toJSON = function () {
    return {
      '_id': this.id,
      'content': {
	'type': 'claim',
	'permanode': this.permanode,
	'issuer': this.issuer,
	'claimType': this.type,
	'sub': this.value,
	'date': this.date.toJSON()
      }
    };
  };


  var ManyClaim = exports.ManyClaim = function (issuer, date, subclaims, id)
  {
    this.id = id;
    this.issuer = issuer;
    this.date = date;
    this.type = "multi";
    this.subclaims = subclaims;
    this.permanodes = subclaims.map (function (i) {
		        return i.permanodes;
	              });
  };

  ManyClaim.prototype = Object.create (NodeHolder.prototype);
  ManyClaim.prototype.toJSON = function () {
    var subdocs = [], sub, i;

    for (i = 0; i < this.subclaims.length; i++) {
      sub = this.subclaims[i].toJSON ().content;
      delete sub.type;
      delete sub.issuer;
      delete sub.date;

      subdocs.push (sub);
    }

    return {
      '_id': this.id,
      'content': {
	'type': 'claim',
	'issuer': this.issuer,
	'claimType': 'multi',
	'date': this.date.toJSON(),
	'sub': subdocs
      }
    };
  };


  /**
   * @namespace
   * @property {SubClaim} subclaim 	- the inner claim and its real data
   * @property {string} type 		- type of the claim
   * @property {Permanode[]} permanodes 	- permanodes related to this claim
   * @property {string} issuer 		- id of the issuer of this claim
   *
   * @class
   * @constructor
   * @param {SubClaim|ManyClaim} subclaim
   * @param {signature_array} signatures
   */
  var Claim = exports.Claim = function (signed, signatures, subclaim)
  {
    SignedNode.call (this, subclaim, signatures);

    this.claim = subclaim;
    this.type = this.claim.type;
    this.permanodes = this.claim.permanodes;
    this.issuer = this.claim.issuer;
    this.date = this.claim.date;
  };

  /**
   * @param {string} identity
   * @param {string} permanode
   * @param {string} type
   * @param {Object} value
   */
  Claim.create_sub = function (identity, permanode, type, value) {
    return new SubClaim (permanode, identity, type, value, new Date());
  };

  /**
   * @param {Identity} identity
   * @param {Permanode|Object} permanode_or_obj
   * @param {string|undefined} type
   * @param {Object|undefined} value
   */
  Claim.create = function (identity, permanode_or_obj) {
    var permanode = permanode_or_obj,
	base, obj, mini, key, signature;

    if (permanode instanceof Permanode) {
      permanode = permanode.id;
    } else if (permanode instanceof SignedNode) {
      permanode = permanode.entity.id;
    }

    if (typeof permanode === "object" && typeof permanode !== "string") {

      obj = permanode_or_obj;
      mini = [];

      for (key in obj) {
	if (obj.hasOwnProperty (key)) {
	  mini.push (Claim.create_sub (identity, key, obj[key]));
	}
      }

      base = new ManyClaim (identity.id, new Date(), mini);

    } else {
      base = Claim.create_sub (identity.id, permanode, arguments['2'], arguments['3']);
    }

    signature = identity.sign (base.toJSON());

    return signature.then (function (signature) {
	     return new Claim (base.toJSON().content, [signature], base);
	   });
  };

  Claim.prototype = Object.create (SignedNode.prototype);


  /**
   * Generates a ClaimSet.
   *
   * A ClaimSet is an specialization of an array to deal with claims and
   * filter them.
   *
   * @param {Array.<Claim>} claims
   */
  var ClaimSet = function ClaimSet (claims)
  {
    if (!(claims instanceof Array)) {
      this.push (claims);
    } else {
      claims.forEach (function (item) {
	this.push (item);
      }, this);
    }
  };

  ClaimSet.prototype = Object.create (Array.prototype);

  /**
   * @private
   * @this {Object} which will hold the properties
   */
  ClaimSet.prototype.$addLatestProps = function addLatestProps (elt) {
    if (elt.type === 'multi') {
      elt.sub.forEach (function (elt) {
	addLatestProps (elt);
      });
    } else {
      if (!this.hasOwnProperty (elt.type)) {
	this[elt.type] = elt;
      }
    }
  };

  ClaimSet.prototype.filterActive = function () {
    var ctx = {}, ctx_items = [], i;


    this.forEach (this.$addLatestProps, ctx);

    for (i in ctx) {
      if (ctx.hasOwnProperty (i)) {
	ctx_items.push (ctx[i]);
      }
    }

    return new ClaimSet (ctx_items);
  };

  /**
   * Return the possible Subclaim(s) of a claim in the context passed
   * on. We will make changes like this[claim.type] = { value:
   * subclaim.value, ...}
   *
   * @this {Object} context passed to us
   * @param {Claim|ManyClaim} claim
   */
  ClaimSet.prototype.$getFlatSubclaims = function addFlatSubclaims (claim, superclaim) {
    /** @type SubClaim */
    var subclaim = claim.claim;

    if (subclaim.type === 'multi') {
      return addFlatSubclaims.call (this, subclaim, claim);
    }

    if (!this.hasOwnProperty (subclaim.type)) {
      this[subclaim.type] = {
	value: subclaim.value,
	part_of: superclaim || claim,
	permanodes: subclaim.permanodes,
	type: subclaim.type
      };
    }
  };


  /**
   * Return the base subclaims that would correspond to the current
   * ClaimSet, destructuring the real claims, and dropping the possible
   * hierarchies in 'multi's.
   */
  ClaimSet.prototype.subclaims = function () {
    var ctx = {};

    this // Closure our inner function so that forEach does not
    // pollute our space
    .forEach ((function ($) {
		 return function (i) {
		   return $.$getFlatSubclaims.call (this, i);
		 };}(this)), ctx);

    return ctx;
  };


  var ContentKind = exports.ContentKind = function (kind, attrlist)
  {
    this.props = attrlist;
    this.props.kind = kind;
  };

  ContentKind.prototype = Object.create(NodeHolder.prototype);

  ContentKind.prototype.toJSON = function () {
    return when({
      "content": this.props
    });
  };

  ContentKind.prototype.getKind = function () {
    return this.props.kind;
  };


  var RichContent = exports.RichContent = Object.create (NodeHolder);


  var WikiText = exports.WikiText = function (permanode, title, body, author, categories)
  {

    var blobs = this.rawBlobs (body),
	self = this, kind, bp, kp;

    kind = when.all(blobs).then (function (blobs) {
	     return new ContentKind ('wikitext', {
	       "title": title,
	       "author": author.id,
	       "blob-set": blobs
	     });
	   });

    bp = when.map (blobs, function (blob) {
	   return  blob.save ();
	 });

    kp = kind.then (function (kind) { return kind.save (); });

    this.bp = bp;
    this.kp = kp;

    if (permanode === false) {
      return; // Don't create permanode
    }

    if (!permanode) {
      // Create the permanode
      permanode = new Permanode ();
      permanode.save ();
    }

    kind.then (function (kind) {
      return kind.getId();
    }).then (function (kind_id) {
      self.claim = new Claim (permanode, author, "set-attribute", { "attribute": "content", "value": kind_id});
      self.claim.save ().then (console.log);
    });
  };

  WikiText.prototype = Object.create (RichContent.prototype);

  /**
   * @class
   * @constructor
   *
   * @param {String} filename
   * @param {BytesRef} bytesref
   * @param {Array|null} xattrs
   */
  var File = exports.File = function File (filename, bytesref, xattrs)
  {
    this.id = null;
    this.filename = filename;
    this.bytesref = bytesref;
    this.xattrs = xattrs;
  };

  File.create = {};
  File.create.from_buffer = function (buffer, filename) {
    // First we need to generate the byterefs

    var chunk = Chunk.create (buffer);

    return chunk.then (function (chunk) {
	     return BlobRef.create (chunk);
	   }).then (function (blobref) {
	     return BytesRef.create([blobref]);
	   }).then (function (bytesref) {
	     var w = when.defer();

	     context.getProto ('nodeStorage', function (storage) {
	       File.create
	       .from_bytesref (bytesref, filename, null)
	       .done (function (file) {
		 file.id = storage.getIdForDocument (file.toJSON ());
		 w.resolve (file);
	       });
	     });
	     return w.promise;
	   });
  };

  File.create.from_bytesref = function (bytesref, filename, xattrs) {
    var w = when.defer();

    context.getProto ('nodeStorage', function (storage) {
      var ref = new File (filename, bytesref, xattrs);
      ref.id = storage.getIdForDocument (ref.toJSON ());
      w.resolve (ref);
    });

    return w.promise;
  };

  File.prototype = Object.create (RichContent.prototype);
  File.prototype.toJSON = function () {
    return {
      "_id": this.id,
      "content": {
	"type": "file",
	"filename": this.filename,
	"bytesRef": this.bytesref.toJSON().content.parts,
	"xattrs": this.xattrs
      }
    };
  };

  return exports;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require, exports, module); });
