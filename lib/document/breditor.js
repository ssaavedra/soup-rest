/*jslint node: true, ass: true, white: true, nomen: true, todo: true, unparam:true, plusplus: true, vars: true */
/*jshint expr: true */
/*global describe: false, before: false, after: false, it: false */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(['assert', 'when', 'when/function', 'events', './identity', './nodes'],
function (assert, when, fn, e, Identity, nodes) {

var Chunk = nodes.Chunk;
var BytesRef = nodes.BytesRef;
var BlobRef = nodes.BlobRef;

var BytesRefEditor = function BytesRefEditor (blobService, newblobs_container) {
	this.svc = blobService;
	this.newblobs = newblobs_container || [];
};

BytesRefEditor.create = fn.lift (function (blobService, newblobs_container) {
	return new BytesRefEditor (blobService, newblobs_container);
});

var totalEdits = 0;
/**
 * @param {[BlobRef]}
 * @param {[Change]}
 * @returns {{refs: [BlobRef], changes: [Change]}}
 */
BytesRefEditor.prototype.editBulk = when.lift (function (refs, changes) {
	if (!changes.length) {
		// console.log ("Base case. Exiting with refs = ", refs);
		return when ({ refs: refs, changes: [] });
	}

	if (totalEdits > 10) {
		throw new Error ("Too many changes!");
	}
	totalEdits++;

	var change = changes[0];

	// console.log ("Current state of things: ", this.printRefs (refs));
	// console.log ("Next Change is: ", change);

	var new_state = change.edit (refs, changes.slice(1));

	return when (this.editBulk (when.all (new_state.refs), new_state.changes));
});

BytesRefEditor.prototype.printRefs = function (refs) {
	var s = '[[:';

	refs.reduce (function (size, cur, idx) {
		s += ":  " + size + "--" + cur.ref.slice(5, 11) + "--" + (size + cur.size) + "  :";
		return size + cur.size;
	}, 0);

	s += ":]]";

	return s;
};


/**
 * Creates an array of Change objects, from an array of JSON objects describing the changes.
 *
 * @param {{type: {String}, value: {Buffer}}} changes
 * @returns {[Change]}
 */
BytesRefEditor.prototype.createChanges = function (changes) {
	var editor = this;
	return changes.map (function (change) {
		return Change.create (change, editor);
	});
};

var Change = {
	create: function (change, editor) {
		if (change.type === '+') {
			return new AdditionChange (change, editor);
		} else if (change.type === '-') {
			return new DeletionChange (change, editor);
		}

		throw new Error ("TYPE OF CHANGE " + change.type + "not handled!", change);
	},
	apply: function (refs) {
		return refs;
	}
};


/** Checks whether position is at the start of a ref */
var nodeWouldNeedToBeSplit = function (blobrefs, position) {
	var i = 0, sum = 0;
	while (i < blobrefs.length && sum < position) {
		sum += blobrefs[i].size;
		i++;
	}

	return sum !== position;
};

/**
 * @param {{}} change_desc - Description for the change (plain object)
 */
var AdditionChange = function (change_desc, editor) {
	this.value = new Buffer (change_desc.value);
	this.pos = change_desc.pos;
	this.len = change_desc.value.length;
	this.editor = editor;
};

AdditionChange.prototype.edit = function (blobrefs, changes) {
	if (nodeWouldNeedToBeSplit (blobrefs, this.pos)) {
		var new_change = new SplitChange (this.pos, this.editor);
		return { refs: blobrefs, changes: ( [new_change, this].concat(changes)) };
	}

	var idx = 0, sum = 0;
	while (sum < this.pos) {
		sum += blobrefs[idx].size;
		idx++;
	}

	var new_node = new FutureNewBlob (this.value, 0, this.value.length, this.editor.newblobs);

	blobrefs = blobrefs.slice (0);
	blobrefs.splice (idx, 0, new_node);

	// Edit changes so that now the values point to their right locations
	var pivot = this.pos;
	var pivot_offset = this.len;
	changes = changes.map (function (change) {
		if (change.pos > pivot) {
			change = Object.create (change);
			change.pos = change.pos + pivot_offset;
		}
		return change;
	});

	return { refs: blobrefs, changes: changes };
};

var SplitChange = function (position, editor) {
	this.pos = position;
	this.editor = editor;
};

SplitChange.prototype.edit = function (blobrefs, changes) {
	var node_start_position = 0, i = 0, node_length;

	while (node_start_position < this.pos && i < blobrefs.length) {
		node_start_position += blobrefs[i].size;
		i++;
	}

	// We should have one more than we should
	i--;
	node_start_position -= blobrefs[i].size;

	var offset = this.pos - node_start_position;

	var first_node = new FutureBlob (blobrefs[i].ref, 0, offset, this.editor.svc, this.editor.newblobs);
	var second_node = new FutureBlob (blobrefs[i].ref, offset, blobrefs[i].size - offset, this.editor.svc, this.editor.newblobs);

	blobrefs = blobrefs.slice (0);
	blobrefs.splice (i, 1, first_node, second_node);

	return { refs: blobrefs, changes: changes };
};




var FutureBlob = function FutureBlob (blobref, start, size, service, newblob_container) {
	this.DEBUG = "FutureBlob";
	this.ref = blobref;
	this.start = start;
	this.size = size;
	this.service = service;
	this.newblobs = newblob_container;

	if (!this.ref) {
		console.trace ();
	}

	this.blob = this.fetch ();
};
FutureBlob.prototype = Object.create ({});

var acceptable_size_difference = function (a, b) {
	return true;
	return Math.abs (Math.abs (a) - Math.abs (b)) < 256;
};

FutureBlob.prototype.fetch = function () {
	var self = this;

	var matching_futureblobs = this.newblobs.filter(function (blob) {
		return blob.id == self.ref;
	});

	var operation_with_blob = function (original) {
		if (self.start === 0 && acceptable_size_difference (self.size, original.size)) {
			return original.content;
		}
		return original.content.slice (self.start, self.start + self.size);
	};

	if (matching_futureblobs.length > 0) {
		return when (this.blob = matching_futureblobs[0]).then (operation_with_blob).then (function (buffer) {
			return Chunk.create (buffer);
		});
	} else {
		return when (this.blob = this.service.getBlob (this.ref)).then (operation_with_blob).then (function (buffer) {
			return Chunk.create (buffer);
		});
	}

};

FutureBlob.prototype.save = function (dao) {
	return this.blob.then (function (blob) {
		return dao.save (blob);
	});
};

FutureBlob.prototype.then = function (callback) {
	var self = this;
	return this.blob.then (function (blob) {
		if (self.start) {
			self.newblobs.push (blob);
		}

		return new FutureBlobRef (self, blob.id, self.size);
	}).then (callback);
};

var FutureNewBlob = function FutureNewBlob (buffer, start, size, newblobs_container) {
	this.DEBUG = "FutureNewBlob";
	this.buffer = buffer;
	this.start = start || 0;
	this.size = size || this.buffer.length;
	this.newblobs = newblobs_container;

	this.blob = Chunk.create (this.buffer);
};
FutureNewBlob.prototype = Object.create ({});
FutureNewBlob.prototype.save = function (dao) {
	var self = this;

	return this.blob.then (function (blob) {
		return dao.save (blob);
	});
};

FutureNewBlob.prototype.then = function (callback) {
	var self = this;
	return this.blob.then (function (blob) {
		self.newblobs.push (blob);
		return new FutureBlobRef (self, blob.id, self.size);
	}).then (callback);
};

var FutureBlobRef = function FutureBlobRef (futureblob, blobid, size) {
	this.DEBUG = "FutureBlobRef";
	this.futureblob = futureblob;
	this.ref = blobid;
	this.refType = "blobRef";
	if (!size || size === undefined) {
		throw new Error ("FutureBlobRef: size is undefined: " + size);
	}
	this.size = size;
};

FutureBlobRef.prototype = Object.create (BlobRef.prototype);

module.exports = BytesRefEditor;

	return BytesRefEditor;
});
