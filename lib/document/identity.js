/*jslint node: true, ass: true, white: true, nomen: true, plusplus: true */
"use strict";

(function(define) { 'use strict';
define(function (require) {

  var assert = require('assert'),
      when = require('when'),
      context = require('../context'),
      generate = require('../identity/generate');

  var Identity = exports.Identity = function(id, name, pubKey, privKey)
  {
    this.id = id;
    this.pubKey = pubKey;
    this.privKey = privKey;
  };

  Identity.generate = function(name) {
    return generate.keypair().then(function(kp) {
	     var i = new Identity (undefined, name, kp.pubKey, kp.privKey),
		 w = when.defer();

	     context.getProto ('nodeStorage', function (storage) {
	       var id = storage.getIdForEntity (i);
	       i.id = id;
	       w.resolve (i);
	     });
	     return w.promise;
	   });
  };

  Identity.prototype.toJSON = function () {
    var o = {
      "content": {
	"type": "identity",
	"name": this.name,
	"pubKey": this.pubKey
      }
    };

    if (this.privKey)
      o.content.privKey = this.privKey;

    return o;
  };

  /**
   * Creates a detached signature to be later incorporated into the
   * document.
   *
   * This function signs the "content" property of doc. Doc *MUST*
   * contain a "content" property.
   */
  Identity.prototype.sign = function(doc) {

    var self = this;

    assert.ok(doc.content);

    return generate.signature(doc.content, this.privKey).then(function(value) {
	     var signature = {
	       'issuer': self.id,
	       'signature': value,

	       /* In the future we might support more encryption methods */
	       'method': 'openssl-native',
	       'mvers': 0
	     };

	     return signature;
	   });

  };

  Identity.prototype.verify = function (doc, sig) {
    // if signature not in peer list
    // what to do?

    var pub = this.pubKey;
    // var signature = find(signature where sig.issuer == this.issuer);
    return generate.verification(doc.content, sig.signature, pub);
  };

  /**
   * Strict checking? Fail when identity not on database, or not, or what
   */
  Identity.verifyAll = function(doc, strict) {
    var i, partials = [],
	verification, sig;

    assert.equal(typeof doc.content, "object");

    if(!doc.meta || !doc.meta.signatures) {
      return false;
    }

    function verify_ok_callback (i) {
      return i.verify (doc);
    }

    function verify_ko_callback () {
      return !strict;
    }

    for (i = 0; i < doc.meta.signatures.length; i++) {
      sig = doc.meta.signatures[i];

      // var verification = this.verifyOther(doc, sig);

      verification = dao
		     .fetch (sig.issuer)
		     .then (verify_ok_callback,
			    verify_ko_callback);

      partials.push(verification);
    }

    return when.all(partials);
  };

  return { Identity: Identity };

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
