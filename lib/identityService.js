/*jslint node: true, ass: true, white: true, nomen: true, unparam: true, plusplus: true */
"use strict";

(function(define) { 'use strict';
define(function (require) {

  var when = require("when"),
      fn = require("when/function"),
      i = require("./document/identity");

  var Identity = i.Identity;

  /**@class */
  var IdentityService = function IdentityService (identityDao) {
    this.dao = identityDao;
  };

  IdentityService.prototype = Object.create ({});

  IdentityService.prototype.getIdentities = function () {
    return this.dao.getAll ();
  };

  IdentityService.prototype.getSignableIdentities = function () {
    var identities = this.dao.getSignable ();
    return identities;
  };


  /**
   * Creates a new identity in the system with the ability to sign as it.
   *
   * @param {string} opt_name	- Optionally set a name to identify it
   */
  IdentityService.prototype.create = function (opt_name) {
    return Identity.generate (opt_name)
      .then (this.dao.save.bind (this.dao));

  };

  /**
   * Gets an Identity by its id
   */
  IdentityService.prototype.get = function (id) {
    return this.dao.fetch (id);
  };

  /**
   * API provided to sign a document externally.
   *
   * @param {string} identity_id	- Identity to sign as
   * @param {{}} document 		- as a javascript object (JSON)
   * @returns {string} the detached signature
   */
  IdentityService.prototype.sign = function (identity_id, document) {
    this.dao.fetch (identity_id).then (function (identity) {
      return identity.sign (document);
    });
  };

  /**
   * Method provided to verify a signature from a document.
   *
   * @param {string} signature
   * @param {string} issuer_id
   * @param {{}} document
   * @returns {bool}
   */
  IdentityService.prototype.verify = function (signature, issuer_id, document) {
    this.dao.fetch (issuer_id).then (function (identity) {
      identity.verify (document, signature);
    });
  };


  return IdentityService;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
