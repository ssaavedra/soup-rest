//
// DHT-Node/Kadoh
//

var Node = require('kadoh/lib/node');
var SHA1 = require('kadoh/lib/util/crypto').digest.SHA1;
var Reporter = require('kadoh/statistics/cube/reporter');
var logging = require('kadoh/lib/logging');
var ConsoleLogger = require('kadoh/lib/logger/reporter/color-console');

new ConsoleLogger(logging, 'error');

var NodeProxy = exports.Node = function(options) {
  options = this._options = options || {
    node       : {},
    delay      : undefined,
    name       : 'unnamed',
    activity   : false,
    values     : 10,
    reporter   : false
  };
  options.node.reactor = options.node.reactor || {};
  options.node.reactor.transport = options.node.reactor.transport || {};
  options.node.reactor.transport.reconnect = true;
  this.kadoh = new Node(null, options.node);
  if (options.reporter)
    this.reporter = new Reporter(this.kadoh);
};

NodeProxy.prototype.start = function() {
  if (this._options.name === 'unnamed') {
    return false;
  }
  setTimeout(function(self) {
    console.log(self._options.name + ' connecting');
    self.connect();
  }, this._options.delay || 1, this);
};

NodeProxy.prototype.connect = function() {
  var self = this;
  this.kadoh.connect(function() {
    self.join();
    if (self.reporter)
      self.reporter.start();
  });
};

NodeProxy.prototype.join = function() {
  var self = this;
  console.log(self._options.name + ' joining');
  this.kadoh.join(function(error) {
    console.log(self._options.name + ' joined', self.kadoh._routingTable.howManyPeers());
    if (self._options.activity) {
      self.randomActivity();
    }
  });
};

NodeProxy.prototype.on = function(event, callback) {
  // TODO: Implement state things
};

NodeProxy.prototype.fire = function(event, value) {
  // TODO: Implement state things
};
