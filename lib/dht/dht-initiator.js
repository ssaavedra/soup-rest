
var logger = require ('../log').getLogger('dht');

var argv = require('optimist')
    .usage('Usage: $0 -p [num] -d [num]')
    .alias('p', 'port')
    .alias('d', 'delay')
    .alias('b', 'bootstraps')
    .argv;

var name      = 'udpbot',
    protocol  = 'mainline', // 'jsonrpc2',
    type      = 'udp',
    delay     = argv.delay || 0,
    transport = {
      port      : parseInt(argv.port, 10) || 10000,
      reconnect : true
    },
    bootstraps = argv.bootstraps.split(',');

var config = {
  name : name,
  node : {
    bootstraps : bootstraps,
    reactor : {
      protocol  : protocol,
      transport : transport
    }
  },
  delay      : delay,
  activity   : false
};

process.env.KADOH_TRANSPORT = type;

var Node = require(__dirname + '/dht-node-kadoh');

var id = "sha1-noone";
var storageProvider = null;

var kadoh = new Node(id, storageProvider, config.node);

kadoh.connect(function() {
  logger.info ("Connected. Joining network...");
  kadoh.join (function (error) {
    logger.info("BOT" + " joined " + kadoh._routingTable.howManyPeers());
  });
});

if (argv.cli) {
  logger.info ("KadOH object is 'node'");
  require('repl').start('> ').context.node = kadoh;

}

var DhtInitiator = module.exports = function (storageProvider) {
  this.initialized = false;
  this.storage = storageProvider;
  this.kadoh = null;
  this.node_id = "-uninitialized-";
};

DhtInitiator.prototype = Object.create ({});

DhtInitiator.prototype.initialize = function () {
  if (initialized) {
    return;
  }
  // TODO: Set this.node_id to something that is useful
  // Grab from this.storage
  this.kadoh = new Node (this.node_id, this.storage, config.node);
};

DhtInitiator.prototype.listen = function (port) {
  this.initialize ();
  // TODO: Something here to be done.
};

DhtInitiator.prototype.connect = function () {
  logger.info ("Connect DHT");
  return this.kadoh.connect.apply (this.kadoh, arguments);
};

DhtInitiator.prototype.join = function () {
  logger.info ("Join DHT");
  return this.kadoh.join.apply (this.kadoh, arguments);
};

DhtInitiator.prototype.disconnect = function () {
  logger.info ("Disconnect DHT");
  return this.kadoh.disconnect.apply (this.kadoh, arguments);
};
