var kadoh              = require('kadoh'),
    StateEventEmitter  = require('kadoh/lib/util/state-eventemitter'),
    Deferred           = require('kadoh/lib/util/deferred'),
    Crypto             = require('kadoh/lib/util/crypto'),
    PeerArray          = require('kadoh/lib/util/peerarray'),
    XORSortedPeerArray = require('kadoh/lib/util/xorsorted-peerarray'),
    IterativeDeferred  = require('kadoh/lib/util/iterative-deferred'),

    globals            = require('kadoh/lib/globals.js'),

    RoutingTable       = require('kadoh/lib/dht/routing-table'),
    Peer               = require('kadoh/lib/dht/peer'),
    BootstrapPeer      = require('kadoh/lib/dht/bootstrap-peer'),

    Reactor            = require('kadoh/lib/network/reactor'),
    PingRPC            = require('kadoh/lib/network/rpc/ping'),
    FindNodeRPC        = require('kadoh/lib/network/rpc/findnode'),
    FindValueRPC       = require('kadoh/lib/network/rpc/findvalue'),
    StoreRPC           = require('kadoh/lib/network/rpc/store'),

    ValueManagement    = require('kadoh/lib/data/value-store');


var SearchPropRPC      = require(__dirname + '/rpc/searchprop');


/**
 * @namespace StorageProvider
 */

/**
 * @function getBlob
 * @param {string} blob_id
 */


/**
 * A Bootstrap Node in the SOUP Network is as follows.
 * @namespace Bootstrap
 */
var Bootstrap = module.exports = kadoh.logic.Bootstrap.extend({
  initialize: function(id, options) {
    this.supr(id, options);
    this.setState('initializing');

    var config = this.config = {};
    for (var option in options) {
      config[option] = options[option];
    }

    this._peers = new PeerArray();

    this._reactor = new Reactor(this, config.reactor);
    this._reactor.register({
      PING       : PingRPC,
      FIND_NODE  : FindNodeRPC,
      FIND_VALUE : FindValueRPC,
      STORE      : StoreRPC,
      SEARCH_PROP: SearchPropRPC
    });
    this._reactor.on(this.reactorEvents, this);

    this.setState('initialized');
  },


  reactorEvents : {
    // Connection
    connected: function(address) {
      console.log ("Connected");
      this._me      = new Peer(address, this._id);
      this._address = address;
      this.setState('connected');
    },

    disconnected: function() {
      console.log ("Disconnected");
      this.setState('disconnected');
    },

    // RPC
    reached: function(peer) {
      console.log ("Reached by ", peer.getAddress(), peer);

      peer.touch();
      console.log('add peers', peer.getAddress());
      this._peers.addPeer(peer);
    },

    queried: function(rpc) {
      console.log ("Queried with RPC", rpc);
      this._handleRPCQuery(rpc);
    }
  }

});
