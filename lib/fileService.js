/*jslint node: true, ass: true, white: true, nomen: true, unparam: true, plusplus: true */

(function(define) { 'use strict';
define(function (require) {
  var when = require('when'),
      log = require('./log'),
      nodes = require('./document/nodes');

  var logger = log.getLogger ('fileService');

function collaterally (callback, ctx) {
	return function () {
		var p = callback.apply (ctx, arguments),
		r = arguments [0];

		if (p.then) {
			return p.then (function () { return r; });
		}
		return r;
	};
}


/**
 * This Service allows the uploading of files.
 *
 * @class
 * @constructor
 * @param {Object} o - An object containing the dependencies of the FileService
 */
var FileService = function FileService (o) {
	this.blobs = o.blobService;
	this.claims = o.claimService;
	this.dao = o.fileDao;
	this.storage = o.nodeStorage;
};


FileService.prototype = Object.create (Object.prototype);

FileService.prototype.createFile = function (bytesref, filename, xattrs) {
	var promise, self = this;
	promise = nodes.File.create.from_bytesref (bytesref, filename, xattrs);
	return promise.then (this.dao.save.bind (this.dao));
};

FileService.prototype.uploadFile = function (buffer, filename, xattrs) {
	var my = this;

	// Here we should compose the usecase by using other services, as such:

	return my.blobs.upload (buffer)
		.then (function (blob) {
			return my.blobs.getBytesRef ([blob]);
		}).then (function (bytesref) {
			return my.createFile (bytesref, filename, xattrs);
		});
};


/**
 * @type Change
 * @prop {String} type
 * @prop {Buffer} value
 * @prop {number} pos
 */
var Change = {};
Change.prototype = Object.create ({});
Change.prototype.type = "";
Change.prototype.value = new Buffer(0);
Change.prototype.pos = 0;


/**
 * Uploads a file via a patch to the original file with new additions
 * and/or deletions.
 *
 * @xparam {nodes.File} origin
 * @param {string} origin_id
 * @param {Array.<Change>} changes
 * @param {Array.{}} xattrs - new xattrs for the file
 */
FileService.prototype.partialUpload = function (origin_id, changes, xattrs) {
	var my = this, newblobs = [], new_attrs = xattrs;

	logger.debug ("PARTIAL UPLOAD WITH ORIGIN ID = ", origin_id);

	return this.get (origin_id).then (function (origin) {
		logger.trace ("PARTIAL UPLOAD WITH ORIGIN = ", origin);
		return when.all([origin, my.blobs.getBytesRefByChanges (origin.bytesref, changes, newblobs, my.blobs)]);
	}).then (function (values) {
		var origin = values[0], bytesref = values[1], q = [];

		newblobs.forEach (function (blob) {
			q.push (my.blobs.upload (blob.content));
		});

		return when.all (q).then (function () {

			if (new_attrs === undefined) {
				new_attrs = origin.xattrs;
			}

			return my.createFile (bytesref, origin.filename, new_attrs);
		});
	});
};

FileService.prototype.setPermanode = function (permanode, file_or_promise) {
	var self = this;

  if (typeof file_or_promise === "string") {
    return this.setPermanode (this.get(file_or_promise));
  }

  return when (file_or_promise)
	 .then (function (file) {
	   return self.claims.setContent (permanode, file);
	 });
};


/**
 * Get a file entity from the identifier, resolving permanode-links
 * whenever needed.
 * @param {string} identifier - identifier of the file or permanode
 * @returns {Promise} of a File object
 */
FileService.prototype.get = function (identifier) {
	// Test if identifier is of file or permanode
	var my = this;

	return this.dao.isFile (identifier).then (function () {
		logger.debug ("GOT A FILE, IT WAS A FILE!");
		return identifier;
	}, function () {
		logger.debug ("GOT A PERMANODE, IT WAS NO FILE!");
		// It was a permanode..
		// Find out the real id
		// 1. Get the claim
	  return my.claims.getByPermanode(identifier, "content")
	    .then (function (claims) {
	           logger.debug ("THESE ARE THE CALIMS: ", claims);
                   for (var x in claims) {
                     logger.trace ("Claim[" + x + "].id = " + claims[x].claim.id);
                     logger.trace ("Claim[" + x + "].attrib = " + claims[x].claim.value.attribute);
                   }

	      if (claims.length === 0) {
		throw new Error("No claims matched. File does not exist.");
	      }

	      var filtered = claims.filter (function (claim) {
		return claim.type === "set-attribute" && claim.claim.value && claim.claim.value.attribute === "content";
	      });

	      return filtered[0]
		.claim.value.value;

	    });

	}).then (this.dao.fetch.bind (this.dao));
};


/**
 * Get the length in bytes of a file.
 * Permanode identifiers get automatically resolved.
 *
 * @param {String} file_id            - The identifier of the file
 *
 * @return number Length of the file
 */
FileService.prototype.getLength = function (file_id) {
	var file = this.get (file_id);

	return file.then (function (file) {
		return file.bytesref.getLength ();
	});
};


/**
 * Get the contents of a file.
 * Permanode identifiers get automatically resolved.
 *
 * @param {string} file_id             - The identifier of the file.
 * @param {number} start_range         - The first byte to get.
 * @param {number|undefined} end_range - The last byte to get. Undefined == EOF
 * @param {FileService~on_callback} on_callback
 */
FileService.prototype._getBytes = function (file, start_range, end_range, ref_start, on_callback) {
	var length = end_range - start_range;
	var emitter = new SortedMessageEmitter (on_callback);
	var q = [];
	var blobService = this.blobs;

	var refs = file.bytesref.withinRange (start_range, end_range);
	var first_ref = refs.refs[0];//slice(0, 1);
	var first_ref_start_offset = start_range - ref_start;

	var end = Math.min (end_range, first_ref.getLength() - first_ref_start_offset);
	q.push (first_ref.getContent (blobService).then (function (blob) {
		return emitter.emit (0, blob.slice (first_ref_start_offset, first_ref_start_offset + end));
	}).catch (function (e) {
		logger.error ("ERROR ERROR ###:", e, blobService, refs[0]);
	}));

	refs.slice(1).forEach (function (ref, idx) {
		var end = ref.getLength ();
		q.push (ref.getContent (blobService).then (function (blob) {
			return emitter.emit (idx + 1, blob.slice (0, end));
		}).catch (function (e) {
			logger.error ("ERROR ERROR ERROR:", e, blobService, ref);
                        return e;
		}));
	});


	return when.all (q).then (function () {
		emitter.emit (refs.length, null);
		return true;
	});
};


/**
 * Returns a promise to the full bytes in the asked range.
 */
FileService.prototype.getBytes = function (file_id, start_range, end_range) {
	var svc = this, buffer;

	start_range = start_range === undefined ? 0 : start_range;
	end_range = end_range === undefined ? -1 : end_range;

	if (end_range === undefined || end_range === -1) {
		return this.getLength (file_id).then  (function (length) {
			return svc.getBytes (file_id, start_range, length);
		});
	}

	return this.get (file_id).then (function (file) {
		var refs = file.bytesref.withinRange (start_range, end_range);
		var first_ref_start = file.bytesref.refStartFromPosition (start_range);
		var io = 0;
		var collector = function (idx, content) {
			content.copy (buffer,
				      io,
				      0,
				      Math.min (buffer.length, end_range - io));
			io += content.length;
		};

		buffer = new Buffer (end_range - start_range);
		// buffer = new Buffer (refs.getLength ());

		return svc._getBytes (file, start_range, end_range, first_ref_start, collector);
	}).then (function () {
		return buffer;
	});
};


/**
 * @class
 * @param {FileService~on_callback} receiver
 */
var SortedMessageEmitter = function (receiver) {
	this.receivers = [];
	this.queuedMessages = [];
	this.nextMessage = 0;

	if (receiver)
		this.receivers.push (receiver);
};


/**
 * @param {FileService~on_callback} receiver - the receiver to add to the list of listeners
 */
SortedMessageEmitter.prototype.addListener = function (receiver) {
	this.receivers.push (receiver);
};


/**
 * @param {FileService~on_callback} receiver - the receiver to remove from the list of listeners
 */
SortedMessageEmitter.prototype.removeListener = function (receiver) {
	delete this.receivers [this.receivers.indexOf (receiver)];
};


/**
 * Sends the message to the emitting queue. The message will be emitted if it is the next in the sequence; if it's not it will be emitted in its own time.
 *
 * @param {number} idx - index of the current message
 * @param {{}} content - content of the message
 */
SortedMessageEmitter.prototype.emit = function (idx, content) {
//	if (this.nextMessage == idx) {
//		this._emit (idx, content);
	if (content === null) {
		for (var i = 0; i < this.queuedMessages.length; i++) {
			this._emit (i, this.queuedMessages[i]);
		}
	} else {
		this.queuedMessages [idx] = content;
	}
	// this.emptyQueue ();
};


/**
 * Actually emit the message and increment the current message
 * counter.
 */
SortedMessageEmitter.prototype._emit = function (idx, content) {
	this.receivers.forEach (function (receiver) {
		if (typeof receiver === "function")
			receiver (idx, content);
	});
	this.nextMessage++;
};


/**
 * Loop through unsent messages and send all the ones that were
 * waiting for a previous message which has just arrived.
 */
SortedMessageEmitter.prototype.emptyQueue = function () {
	var idx, content;

	while (this.nextMessage in this.queuedMessages) {
		idx = this.nextMessage;
		content = this.queuedMessages [idx];

		this._emit (idx, content);
		delete this.queuedMessages [idx];
	}
};

/**
 * This callback is to be passed in FileService#getBytes(), so that
 * whenever new bytes are available they are received by the calling
 * part without having to wait for the whole picture and without even
 * needing to load the full file in memory. Note that the content is
 * returned already sorted (because it goes through an internal
 * SortedMessageEmitter).
 *
 * @callback FileService~on_callback
 * @param {number} index of message - for ordering
 * @param {{}} content of the message
*/


	return FileService;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });

