/*jslint node: true, ass: true, white: true, nomen: true, unparam: true, plusplus: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var kadoh = require('kadoh'),
      Node = require('./dht/dht-node-kadoh'),
      config = require('config'),
      when = require('when'),
      callbacks = require('when/callbacks');

  var name      = 'udpbot',
  protocol  = 'mainline', // 'jsonrpc2',
  type      = 'udp',
  delay     = 0,
  transport = {
    port      : 10000,
    reconnect : true
  },
  bootstraps = ['127.0.0.1:3001', '127.0.0.1:3002'];

  var xconfig = {
    name : name,
    node : {
      bootstraps : bootstraps,
      reactor : {
	protocol  : protocol,
	transport : transport
      }
    },
    delay      : delay,
    activity   : false
  };

  process.env.KADOH_TRANSPORT = type;

  var generate_id = function () {
    var string = '', i, r,
    chars = '0123456789ABCDEF',
    length = 40;

    for (i = 0; i < length; i++) {
      r = Math.floor (Math.random () * chars.length);
      string += chars.substring (r, r + 1);
    }
    return string;
  };

  var DhtService = module.exports = function DhtService (restServer) {
    this.kadoh = null;
    this.rest_server = restServer;
  };

  DhtService.prototype.initialize = function (requested_id, storage) {
    var new_id;

    if (!requested_id) {
      new_id = generate_id ();
    } else if (requested_id && this.kadoh && this.kadoh.getID() == requested_id) {
      new_id = requested_id;
    } else {
      return when.reject (new Error ("Requested ID does not match previous ID"));
    }

    process.env.KADOH_TRANSPORT = config.get("dht.node.reactor.transport");

    this.kadoh = new Node (new_id, storage, config.get("dht.node"));

    return when (new_id);
  };

  DhtService.prototype.getId = function () {
    if (!this.kadoh)
      return false;

    return this.kadoh.getID();
  };

  DhtService.prototype.getState = function () {
    if (!this.kadoh)
      return undefined;

    return this.kadoh.getState();
  };

  DhtService.prototype.connect = function () {
    if (!this.kadoh)
      return when.reject (false);
    return callbacks.call (this.kadoh.connect.bind (this.kadoh));
  };

  DhtService.prototype.disconnect = function () {
    if (!this.kadoh)
      return when.reject (false);
    return callbacks.call (this.kadoh.disconnect.bind (this.kadoh));
  };

  DhtService.prototype.join = function () {
    if (!this.kadoh)
      return when.reject (false);
    return callbacks.call (this.kadoh.join.bind (this.kadoh)).then (function (e) {
      this.on_join (e);
      return e;
    }.bind (this));
  };

  DhtService.prototype.set = function (key, value) {
    if (!this.kadoh)
      return when.reject (false);

    var exp = 300;

    console.log ("[DHT] [SET (exp=" + exp + ")]", key, "=", value);

    return callbacks.call (this.kadoh.put.bind (this.kadoh), key, value, exp);
  };

  DhtService.prototype.get = function (key) {
    if (!this.kadoh)
      return when.reject (false);

    return callbacks.call (this.kadoh.get.bind (this.kadoh), key).then (function (value) {
      if (value === null)
	throw new Error ("Could not get key " + key);
      return value;
    });
  };

  DhtService.prototype.on_join = function () {
    if (!this.rest_port.port) {
      var config = require('config');
      this.rest_server.init(config.http);
    }

    var node_desc = {
      id: this.getId(),
      rest_port: this.rest_server.port
    };

    this.set (this.getId(), node_desc);
  };

  return DhtService;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
