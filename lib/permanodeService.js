/*jslint node: true, ass: true, white: true, nomen: true, unparam: true, plusplus: true */
"use strict";

(function(define) { 'use strict';
define(function (require) {

  var when = require("when"),
      fn = require("when/function"),
      nodes = require("./document/nodes");

  /** @class
   * @param {PermanodeDao} permanodeDao
   * @param {IdentityService} identityService
   */
  var permanodeService = function PermanodeService (permanodeDao, identityService) {
    this.dao = permanodeDao;
    this.identities = identityService;
  };

  permanodeService.prototype = Object.create ({});

  /**
   * Fetches a Blob from the server, if we have it.
   * @return promise to a blob or to some error
   */
  permanodeService.prototype.getPermanode = function (permanode_id, callback, callback_err) {
    var promise = this.dao.fetch (permanode_id);
    if (callback) {
      promise.then (callback);
    }

    if (callback_err) {
      promise.catch (callback_err);
    }

    return promise;
  };

  /**
   * If creator_identity_id is an identity with a known private key,
   * create a new permanode as such an identity.
   *
   * @param {string} creator_identity_id
   */
  permanodeService.prototype.create = function (creator_identity_id)
  {
    var dao = this.dao;

    return this.identities.get (creator_identity_id).then (function (creator_identity) {
      return nodes.Permanode.create (creator_identity);
    }).then (function (permanode) {
      return dao.save (permanode);
    });
  };


  return permanodeService;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
