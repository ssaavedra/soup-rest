/*jslint node: true, ass: true, white: true, nomen: true, unparam: true, plusplus: true */
"use strict";

(function(define) { 'use strict';
define(function (require) {

  var when = require("when"),
      fn = require("when/function"),
      log = require("./log"),
      nodes = require("./document/nodes");

  var logger = log.getLogger ('blobService');

/**
 * @class
 * 
 * @param {BlobDao} blobDao
 * @param {GenericDao} genDao
 */
var blobService = function BlobService (blobDao, genDao) {
	this.dao = blobDao;
	this.gdao = genDao;
};

blobService.prototype = Object.create ({});

/**
 * Fetches a Blob from the server, if we have it.
 * @return promise to a blob or to some error
 */
blobService.prototype.getBlob = function (blobId, callback, callback_err) {
	var promise = this.dao.fetch (blobId);
	if (callback) {
		promise.then (callback);
	}

	if (callback_err) {
		promise.catch (callback_err);
	}

	return promise;
};


/**
 * Returns the type of an identifier.
 * @returns {string} type of identifier
 */
blobService.prototype.getType = function (blobId, callback, callback_err) {
  var promise = this.gdao.fetch (blobId).then (function (ent) {
                  return ent.toJSON().content.type;
                });

  if (callback) {
    promise.then (callback);
  }

  if (callback_err) {
    promise.catch (callback_err);
  }

  return promise;
};

/**
 * Uploads a Blob to the server.
 * @return promise that will resolve to the Blob generated entity
 */
blobService.prototype.upload = function (content) {
	var ref = new nodes.Chunk (null, content), fetched, self = this;
	ref.id = this.dao.db.getIdForDocument (ref.toJSON());

	return this.dao.save (ref); // .tap (function (i) {
		// logger.info ("SAVED BLOB ", i);
	// });
};


/**
 * Get a BytesRef from a prescription such as:
 *
 * The object must be a plain javascript object with these properties,
 * or an Object with a method .toBlobRef() which should return that
 * javascript.
 *
 * [ { blobRef: "sha1-blobid", size: "100" },
 *   { bytesRef:"sha1-existing-bytesref", size: "200" } ]
 *
 * @param {Object[]} blobrefs
 */
blobService.prototype.getBytesRef = function (blobrefs) {
	blobrefs = blobrefs.map (function (item) {
		return nodes.BlobRef.create_from_json (item);
	});

	var bytesref = nodes.BytesRef.create (blobrefs), self = this;

	return bytesref;
};


/**
 * Get a BytesRef via a transformation from a previous
 * BytesRef. Returns the a promise to the new BytesRef, and in the
 * newblobs array, the created to-be-uploaded blobs.
 *
 * @param {nodes.BytesRef} prev
 * @param {Array.<{}>} changes
 * @param {[]} newblobs - Blobs to get uploaded
 * @return BytesRef new bytesref
 */
blobService.prototype.getBytesRefByChanges = function (prev, changes, newblobs) {
	var editor = new (require ('./document/breditor')) (this, newblobs);

	changes = editor.createChanges (changes);

	var f = editor.editBulk (prev.refs, changes);

	return f.then (function (f) {
		var refs = f.refs;
		return new nodes.BytesRef (refs);
	});
};


/**
 * Get the BytesRef with the actual corresponding values for the blobs
 * they should carry.
 *
 * @param {nodes.BytesRef} bytesref
 */
blobService.prototype.preloadBytesRef = function (bytesref) {
	var i;

	for (i = 0; i < bytesref.refs.length; i++){
		bytesref.refs[i].content = this.getContentFromBlobRef (bytesref.refs[i]);
	}

	return bytesref;
};

/**
 * Get content from an inner BlobRef/BytesRef
 *
 * @param {nodes.BlobRef} bytesref
 */
blobService.prototype.getContentFromBlobRef = function (blobref) {
	var i;

	if (blobref.refType == "blobRef") {
		// It's a final ref
		return this.getBlob (blobref.ref).then (function (blob) {
			return blob.content.slice (0, blobref.content.bytes || blob.content.length);
		});
	} else {
		// Not implemented yet
		logger.debug ("The blobref was %s", blobref);
		throw new Error ("Not implemented yet for type " + blobref.refType);
	}
};

/**
 * Returns a permanent BytesRef id by saving it to the database.
 * @return BytesRef
 */
blobService.prototype.getPermanentBytesRef = function (blobrefs) {
	var dao = this.dao;

	return this.getBytesRef (blobrefs).then (this.dao.save.bind (this.dao));
};

module.exports = blobService;

return blobService;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
