/*jslint node: true */

(function (define) { 'use strict';
define(function (require) {
  var log4js = require("log4js"),
      config = require("config");

  log4js.loadAppender ('file');
  log4js.addAppender(log4js.appenders.file('logs/main.log'), 'soupd');

  var logger = log4js.getLogger ('soupd');
  logger.setLevel (config.log.level);

  return log4js;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
