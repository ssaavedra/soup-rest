/*jslint node: true, white: true, plusplus: true */
"use strict";

(function (define){ 'use strict';
define(function (require) {
  var when = require('when'),
      nodes = require('./document/nodes');

  /**
   * This Service allows for claim management.
   *
   * @constructor
   * @param {Object} o - An object containing the dependencies of the ClaimService
   */
  var ClaimService = function ClaimService (o) {

    /**
     * @member {ClaimDao}
     */
    this.dao = o.claimDao;

    /**
     * @member {IdentityService}
     */
    this.identities = o.identityService;
  };

  ClaimService.prototype = Object.create (Object.prototype);

  /**
   * Gets all the available claims recorded for a given permanode. This
   * should only include "active" claims, not superseeded by others.
   *
   * Note that a ClaimSet (which is returned by this function) is an
   * specialization of an Array.
   *
   * @param {string} permanode - permanode id
   * @returns Promise(ClaimSet) of Claims (signed)
   */
  ClaimService.prototype.getByPermanode = function (permanode, attribute_optional) {
    var claims = this.dao.fetchByPermanode (permanode, attribute_optional);
    return claims;

    claims.then (function (claims) {
      var claimset = new nodes.ClaimSet (claims);
      return claimset.active();
    });
    return claims;
  };

  /**
   * This gets all the available claims for a given permanode, as cached
   * by the current server. This includes claims that may be superseeded
   * by others.
   *
   * @returns Array of Claims (signed)
   */
  ClaimService.prototype.getAllByPermanode = function (permanode) {
    var claims = this.dao.fetchAllByPermanode (permanode);
    return claims;
  };

  ClaimService.prototype.getById = function (claim_id) {
    return this.dao.fetch (claim_id);
  };

  /**
   * Returns a collection of current claimed things, unnested.
   *
   * In a structure such as { key: { value: value, meta: ???}, key: { value: value, meta: ???} }
   */
  ClaimService.prototype.getClaimStatus = function (permanode, callback) {
    var p;

    p = this.getByPermanode (permanode)
      .then (function (claimset) {
	return claimset.subclaims();
      });

    if (callback) {
      p.then (callback);
    }

    return p;
  };

  /**
   * Returns the id of the content pointed by this permanode.  If the
   * permanode is not found, it is assumed to be the content and it is
   * returned unchanged.
   * @param {string} permanode - Id of the permanode
   * @returns {Promise} content
   */
  ClaimService.prototype.retrieveContentId = function (permanode) {
    return this.getClaimStatus (permanode).then (function (o) {
      return o['set-content'].value;
    });
  };

  /**
   * Finds claims (and thus, permanodes) by name, where name is set by a
   * claim of type "set-title"
   *
   * Returns a list of ids to permanodes which contain this title.
   * @param {string} query - Query to search for
   * @returns {Promise} content
   */
  ClaimService.prototype.findByName = function (query) {
    return when(this.dao.findByName (query)).then (function (claims) {
      return claims; // .reduce(function (nodes, claim) { return nodes.concat (claim.permanodes); }, []);
    });
  };

  ClaimService.prototype.findByCustomAttribute = function (attribute, value) {
    return when(this.dao.findByCustomAttribute (attribute, value))
           .then(function (claims) {
             return claims;
           });
  };

  ClaimService.prototype.setName = function (permanode, name, creator_id) {
    return this.addCustom (permanode, "set-attribute", { 'attribute': 'title', 'value': name }, creator_id);
  };

  /**
   * @param {string} permanode_id
   * @param {string} content_id
   * @param {string} creator_id
   * @param {} callback
   * @returns {Promise} to a claim
   */
  ClaimService.prototype.setContent = function (permanode_id, content_id, creator_id, callback) {
    var claim_p, id;

    return this.addCustom (permanode_id, "set-attribute", { 'attribute': 'content', 'value': content_id }, creator_id, callback);
  };


  ClaimService.prototype.addCustom = function (permanode, type, content, creator_id, callback) {
    var claim_p;

    claim_p = this.identities.get (creator_id).then (function (creator) {
      return nodes.Claim.create (creator, permanode, type, content);
    }).then (this.dao.save.bind (this.dao));

    if (callback) {
      claim_p.then (callback);
    }

    return claim_p;
  };


  /**
   * Request a claim to be signed by another identity
   */
  ClaimService.prototype.sign = function (claim_id, identity_id, callback) {
    var claim_p;

    claim_p = when.all ([this.dao.fetch (claim_id),
                         this.identities.get (identity_id)])
              .then (function (values) {
                var claim = values[0],
                    identity = values[1];

                return when.all([claim, identity.sign (claim.toJSON())]);

              }).then(function (values) {
                var claim = values[0],
                    signature = values[1];

                if (claim.signatures.filter (function (sig) {
                      return sig.issuer ===  identity_id;
                    }).length > 0) {
                  // Already a signature from this issuer
                  throw new TypeError("Document already signed");
                }

                claim.signatures.push (signature);
                return claim;
              }).then (this.dao.save.bind (this.dao));

    if (callback) {
      claim_p.then (callback);
    }

    return claim_p;
  };

  /**
   * Request a deletion for a node
   *
   * @param {nodes.NodeHolder} node_id
   * @param {Identity} creator
   */
  ClaimService.prototype.delete = function (node_id, creator) {
    var p = nodes.Claim.create (creator, node_id, 'delete', 'permanode');
    return p.then (this.dao.save.bind (this.dao));
  };

  module.exports = ClaimService;

  return ClaimService;
});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
