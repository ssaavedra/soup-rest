/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

// FIXME: Should not need to require DB directly, but work with Services/DAOs
define(['./basePlugin', '../../couchdb-db', '../../document/identity'], function (BasePlugin, db, i) {

  var Identity = i.Identity;

  var Plugin = function () {
    BasePlugin.call (this, 'identities');

  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.load_service ('claimService');
    this.load_service ('identityService');
  };


  Plugin.prototype.views = {};
  Plugin.prototype.views.list_signatures = function () {
    // List all keys you can sign with
    // aka proxy the request to couch
    var res = this.res, context = this;

    this.services.identityService.getSignableIdentities().then(function (docs) {
      if (docs.length == 0) {
        throw new Error("No identities found");
      }
      docs = docs.map (function (doc) {
	return doc.id;
      });

      context.utils.return_json (res, 200, docs);
    }).catch(function (docs) {
      context.utils.return_json (res, 404, docs);
    });

  };

  Plugin.prototype.views.signature_get =  function (keyId) {
    var id = new Identity(null, keyId);
    id.keyPair.then(this.utils.return_json.bind (this.utils, this.res, 200),
		    this.utils.return_json.bind (this.utils, this.res, 404));
  };


  var checkSignPreconditions = function (doc) {
    // If the last two signatures are the same, don't update the document
    var signatures = doc.meta.signatures, s1, s2;

    // Precondition is OK if this is the first signature
    if (signatures.length < 2) {
      return doc;
    }

    s1 = signatures[signatures.length - 2];
    s2 = signatures[signatures.length - 1];

    console.log(signatures, s1, s2);
    if (s1 && s2 && s1.issuer === s2.issuer) {
      console.log("Signatures were equal");
      doc.meta.signatures.pop();
      throw 412;
    }

    return doc;
  };

  var signDocHandler = function (doc, id) {
    return id.sign(doc).then(function (doc) {
      return checkSignPreconditions(doc);
    });
  };

  /**
   * Test:
   * curl -X POST http://127.0.0.1:8124/sign/93a36de139334aedb6dd9a4afa00087e -H 'Content-Type: application/json' -d '{ "content": { "this": "rocks" } }'
   */
  Plugin.prototype.views.sign_posted = function (keyId) {
    var doc = '', res = this.res;

    this.req.on('data', function (chunk) {
      doc += chunk.toString();
    });

    this.req.on('end', function () {
      var id = new Identity(null, keyId);
      doc = JSON.parse(doc);

      signDocHandler(doc, id)
	.then(this.utils.jsonify(res, 200),
	      this.utils.jsonify(res, 412));
    });
  };

  /** TODO: The architecture to sign documents */
  /** FIXME: This is not correct */
  var loadDocFromDb = function (then) {
    return function (keyId, database, docId) {
      return db.db(database).get(docId).then(function (doc) {
	if (!doc) {
	  throw 404;
	}

	var id = new Identity(null, keyId);
	return then(doc, id);
      });
    };
  };

  var saveDocToDb = function (then) {
    return function (keyId, database, docId) {
      var doc_p = then.apply(this, arguments);

      return doc_p.then(function (doc) {
	return db.db(database).save(doc).then(undefined, function (err) {
	  console.log(err);
	  throw 500;
	});
      });
    };
  };

  var returnAsJson = function (then, status_ok, status_err) {
    return function () {
      var self = this,
      obj = then.apply(this, arguments),
      ok = status_ok || 200,
      ko = status_err || 400;

      return obj.then(this.utils.jsonify(self.res, ok),
		      this.utils.jsonify(self.res, ko));
    };
  };


  var signDocInPlace = returnAsJson (saveDocToDb (loadDocFromDb (signDocHandler)));
  var signDocProbe = returnAsJson (loadDocFromDb (signDocHandler));


  var _verifyAllSigsForDoc = function (res, doc) {
    var id = new Identity();

    if (id.verifyAll(doc)) {
      this.utils.return_json(res, 200, { verified: doc.meta.signatures });
    } else {
      this.utils.return_json(res, 404, { safe: false });
    }
  };

  var verifyAllSigsForDoc = function () {
    var doc = '', res = this.res;

    this.req.on('data', function (chunk) {
      doc += chunk.toString();
    });

    this.req.on('end', function () {
      doc = JSON.parse(doc);
      return _verifyAllSigsForDoc(res, doc);
    });
  };

  var verifyAllSigsForDocId = function (dbname, docId) {
    var res = this.res, doc_promise = db.db(dbname).get(docId);

    return doc_promise.then(function (doc) {
      return _verifyAllSigsForDoc(res, doc);
    });
  };


  var createIdentity = function (name) {
    var res = this.res, id;

    if (name === 'create') {
      return this.utils.return_json(res, 403, { error: "Forbidden name" });
    }

    id = this.services.identityService.create (name);
    return id.then(function (doc) {
             this.utils.return_json(res, 201, doc);
             return doc;
           }.bind(this), function (err) {
                console.warn("Cant create identity", err);
                return this.utils.return_json(res, 400, err);
              }.bind(this));
  };


  Plugin.prototype.set_routes = function (router, return_json) {
    var plugin = this;

    var R = function (cb) {
      /**
       * Wrap `this` with the services from the plugin
       */
      return function () {
        var context = Object.create (this);
        context.plugin = plugin;
        context.services = plugin.services;
        context.utils = plugin.utils;
        cb.apply (context, arguments);
      }
    };

    router.get  ('/sign', R(this.views.list_signatures));
    router.get  ('/sign/:keyId', R(this.views.signature_get));
    router.post ('/sign/:keyId', { stream: true }, R(this.views.sign_posted));
    router.put  ('/sign/:keyid/:db/:doc', R(signDocInPlace));
    router.post ('/sign/:keyid/:db/:doc/update', R(signDocInPlace));
    router.post ('/sign/:keyid/:db/:doc/probe', R(signDocProbe));
    router.get  ('/verify/:db/:doc', R(verifyAllSigsForDocId));
    router.post ('/verify/in_place', { stream: true}, R(verifyAllSigsForDoc));
    router.post('/create/:name', R(createIdentity));
    router.get('/create/:name', R(createIdentity));

  };


  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };


});
