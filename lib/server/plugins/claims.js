/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(['./basePlugin', '../../document/identity'],
       function (BasePlugin, i) {

  var Identity = i.Identity;

  var Plugin = function () {
    BasePlugin.call (this, 'claims');
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.load_service ('blobService');
    this.load_service ('identityService');
    this.load_service ('claimService');
  };


  Plugin.prototype.set_routes = function (router, return_json) {
    var plugin = this;

    router.get ('/search/by_name/:query', function (query) {
      plugin.services.claimService
	.findByName (query)
	.then (this.utils.jsonify(this.res, 200));
    });

    router.get ('/p/:permanode', function (permanode) {
      // Get the claims associated with a permanode
      plugin.services.claimService
      .find
    });

    router.post ('/p/:permanode/name', function (permanode) {
      // This function must be authenticated

      plugin.services.claimService
	.setName (permanode, name, creator)
	.then (this.utils.jsonify (this.res, 200));
    });

    router.post ('/p/:permanode/content', dec_authenticated (function (user, permanode) {
      plugin.services.claimService
      .setContent (permanode, content, creator)
      .then (this.utils.jsonify (this.res, 200));
    }));

  };

  function parseCookies (request) {
    return []; // FIXME
    var list = {},
    rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
      var parts = cookie.split('=');
      list[parts.shift().trim()] = unescape(parts.join('='));
    });

    return list;
  }

  var dec_authenticated = function (decorated_function) {

    return function () {
      plugin.logger.trace (this, arguments);

      var cookies = parseCookies (this.req), user = false, ctx = this;

      if ("IDENTITY" in cookies) {
        user = cookies.IDENTITY;
      }

      user = false;

      var args = Array.prototype.slice.call (arguments);
      if (user) {
	return decorated_function.apply (ctx, [user].concat (args));
      } else {
	// TODO: Show "not logged in" message
	this.res.writeHead (401);
	this.res.end ("Not logged in. Error 401");
	throw new Error ("Not logged in for authenticated action");
      }
    };

  };


  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };

});
