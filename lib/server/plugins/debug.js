/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function (require) {

  var config = require ('config');
  var BasePlugin = require ('./basePlugin');
  var de = require('director-explorer');



  var Plugin = function () {
    BasePlugin.call (this, 'debug');
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
  };


  Plugin.prototype.set_routes = function (router) {
    var plugin = this;

    if (config.get ('debug')) {
      console.log ("Debug plugin enabled as per config settings");

      router.get ('/routes', function (query) {
	console.log ("DEBUG: Requested routing table.");
	this.res.end(de.table (router));
      });

    } else {
      console.log ("Debug plugin not exporting routes because debug is disabled in config");
    }
  };
  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };

});
