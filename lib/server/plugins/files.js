/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function (require) {
  var BasePlugin = require ('./basePlugin');

  var Plugin = function () {
    BasePlugin.call (this, 'files');
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.load_service ('claimService');
    this.load_service ('fileService');
  };


  Plugin.prototype.set_routes = function (router) {
    var plugin = this;

    router.get ('/search/by_name/:query', function (query) {
      plugin.services.claimService
	.findByName (query)
	.done (plugin.utils.jsonify (this.res, 200),
	       plugin.utils.error500.bind (this));
    });

    router.get ('/get/:blobId', function (blobId) {
      var fileService = plugin.services.fileService;
      var start = 0, end = -1, matches = null;

      var length = fileService.getLength (blobId);

      /*
       * Accept range header
       */
      if ("range" in this.req.headers) {
	var bytes = this.req.headers.range;
	var re = /^bytes[= ](\d+)-(\d+)?(\/(\d+|\*))?$/;
	matches = re.exec (bytes);
      }

      /*
       * Tell the browser we understand Range: bytes
       */
      this.res.setHeader ("Accept-Ranges", "bytes");

      /*
       * If we have a valid Range header:
       */
      if (matches !== null) {
	start = parseInt (matches[1]) || 0;
	end = parseInt (matches[2]) + 1 || -1;

	length = length.then (function (totalLength) {

	  /* If the range is impossible to satisfy */
	  if (end > totalLength) {
	    plugin.logger.trace ("end = ", end, "; total = ", totalLength);
	    this.res.writeHead (416, { "Content-Range": "bytes */" + totalLength });
	    this.res.end (JSON.stringify({error: "Requested range not satisfiable"}));
	    return;
	  }

	  var realEnd = end == -1 ? totalLength : Math.min (end, totalLength - 1);

	  this.res.setHeader ("Content-Range",
			      ("bytes 1-2/3"
			       .replace ('1', start)
			       .replace('2', realEnd)
			       .replace('3', totalLength)));

	  this.res.setHeader ("Content-Length", realEnd - start + 1);
	  this.res.writeHead (206);
	}.bind (this));
      } else {
	/* It's a plain no-ranges or syntactically-invalid-ranges request */
	length = length.then (function (length) {
	  this.res.setHeader ("Content-Length", length);
	}.bind (this));
      }

      var content = length.then (function () {
	return fileService.getBytes (blobId, start, end);
      });

      content.then (this.res.end.bind (this.res)).catch (function (e) {
        plugin.logger.debug ("Requested file " + blobId + " not found");
        this.res.writeHead(404);
        this.res.end('No content');
      }.bind(this));
    });
  };

  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };

});
