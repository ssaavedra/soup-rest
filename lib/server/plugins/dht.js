/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(function (require) {

  var config = require ('config');
  var BasePlugin = require ('./basePlugin');

  var Plugin = function () {
    BasePlugin.call (this, 'dht');
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.load_service ('identityService');
    this.load_service ('blobService');
    this.load_service ('claimService');
    this.load_service ('permanodeService');
    this.load_service ('dhtService');
  };

  var get_query_string = function (url) {
    var qsraw, qarray;

    if (url.indexOf ('?') !== -1) {
      qsraw = url.split ('?')[1];
      qarray = qsraw.split ('&');
      return qarray;
    } else return [];
  };

  var get_query_parameters = function (url) {
    var qarray, qmap = {}, i, k, v;
    qarray = get_query_string (url);
    if (!qarray) return {};

    for (i = 0; i < qarray.length; i++) {
      v = qarray[i].split('=');
      k = v[0];

      if (k in qmap) {
	// Transform it into an array
	if (!(qmap[k] instanceof Array)) {
	  qmap[k] = [ qmap[k] ];
	}

	qmap[k].push (v[1]);
      } else {
	qmap[k] = v[1];
      }
    }

    return qmap;
  };

  var quote_regexp = function (str) {
    return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
  };

  var auto_redir = function (dst, req, res, code, src) {
    var re = new RegExp ("^(\/.*?)" + quote_regexp (src) + "(\/.*)?$");
    var prefix = req.url.replace(re, '$1');
    var location = dst.replace ('$', prefix);

    res.writeHead (code || 302, { Location: location });
  };


  Plugin.prototype.set_routes = function (router) {
    var plugin = this;

    router.get ('/init', function () {

      plugin.services.dhtService.initialize ().then (function (response) {

	auto_redir ('$/connect', this.req, this.res, 302,'/init');
	this.res.end ("");
      }.bind (this));
    });

    router.get ('/init/:id', function (id) {

      plugin.services.dhtService.initialize (id).then (function (response) {
	this.res.end (response);
      }.bind (this)).catch (function (e) {
	// The ID may not be assigned, and then we sould redirect to the normal init and grab a new id

	auto_redir ('$/init', this.req, this.res, 302, '/init');
      }.bind (this));
    });

    router.get ('/status', function () {
      var dhtService = plugin.services.dhtService;
      var id = dhtService.getId ();

      if (id === false && config.get ("dht.status.auto_connect")) {
      var prefix = this.req.url.replace(/^\/(.*)\/status.*$/, '$1');
      var location_init = "/prefix/init".replace ('prefix', prefix);

	this.res.setHeader ("X-Redirect-Cause", "not-connected-yet");
	auto_redir ('$/init', this.req, this.res, 302, '/status');
	this.res.end ("");
      }

      var state = dhtService.getState ();

      var info = {
	id: id,
	state: state
      };

      this.res.end (JSON.stringify (info));
    });

    router.get ('/connect', function () {
      plugin.services.dhtService.connect ().then (function () {
	return plugin.services.dhtService.join ();
      }).then (function (response) {
	var prefix = this.req.url.replace(/^\/(.*)\/connect.*$/, '$1');
	var location_status = "/prefix/status".replace ('prefix', prefix);

	this.res.writeHead (302, { Location: location_status });
	this.res.end (JSON.stringify (response));
      }.bind (this)).catch (function (err) {
	this.res.writeHead (500);
	this.res.end (JSON.stringify ({errorCode: 500, msg: "Cannot connect", exception: err}));
      }.bind (this));
    });

    router.get ('/disconnect', function () {
      plugin.services.dhtService.disconnect ().then (function (response) {
	this.res.end (JSON.stringify (response));
      }.bind (this)).catch (function (err) {
	this.res.writeHead (500);
	this.res.end (JSON.stringify ({errorCode: 500, msg: "Cannot connect"}));
      }.bind (this));
    });
  };

  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };

});
