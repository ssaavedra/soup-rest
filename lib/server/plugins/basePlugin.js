/*jslint node: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {
  var when = require('when'),
      log = require('../../log');

  /**
   * @constructor
   * @param {string} pluginName
   * @param {load_svc_callback} load_service
   */
  var BasePlugin = function (pluginName) {
    this.$load_service = null;
    this.q = [];
    this.services = {};
    this.logger = log.getLogger ('http.plugin.' + pluginName);
  };

  /**
   * Returns whether the plugin has been totally initialized,
   * i.e., all depending services are loaded.
   *
   * @returns {bool} services have been injected
   */
  BasePlugin.prototype.is_initialized = function () {
    return this.q.length !== 0;
  };

  BasePlugin.prototype.set_service_loader =function (callback) {
    this.$load_service = callback;
  };

  BasePlugin.prototype.load_service = function (svcname) {
    this.q.push(svcname);
    var promise = this.$load_service (svcname);
    var q = when.defer ();

    return promise.then (function (val) {
            delete this.q[this.q.indexOf(svcname)];
            this.q = this.q.slice(0);
            this.services[svcname] = val;
            q.resolve(val);
            return val;
    }.bind(this));
 };

  return BasePlugin;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
