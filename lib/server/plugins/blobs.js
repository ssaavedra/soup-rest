/*jslint node: true */
"use strict";

if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(['./basePlugin', '../../document/identity'], function (BasePlugin, i) {

  var Identity = i.Identity;

  var Plugin = function () {
    BasePlugin.call (this, 'blobs');
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.load_service ('blobService');
    this.load_service ('identityService');
  };


  Plugin.prototype.set_routes = function (router, return_json) {
    var plugin = this;

    router.get ('/blob/:blobid', function (blobId) {
      plugin.services.blobService
	.getBlob (blobId)
	.then (this.utils.jsonify(this.res, 200));
    });

  };

  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance)
  };

});
