/*jslint node: true */
"use strict";

(function (define) { 'use strict';
define(function (require) {

  var config = require('config');
  var when = require ('when');
  var BasePlugin = require ('./basePlugin');
  var vm = require('vm');

  var Plugin = function () {
    BasePlugin.call (this);
  };

  Plugin.prototype = Object.create (BasePlugin.prototype);

  Plugin.prototype.initialize = function (utils) {
    this.set_service_loader (utils.load_service);

    this.utils = utils;
    this.loaded_apps = {};
    this.router = null;
    this.load_service ('fileService');
  };

  Plugin.prototype.load_app = function (app_name, router, res, callback) {
    var app_path = config.apps.path;
    var context = this;

    if ('$' == app_path[0]) {
      app_path = __dirname + "/../../.." + app_path.slice(1);
    }

    app_path = app_path + "/" + app_name; // + "/app.js";

    // Delete the cache if it exists
    try {
      delete require.cache[require.resolve(app_path)];
    } catch (e) {
      console.log ("Error while deleting node_require cache in loading app ", app_path);
    }

    var plugin;
    try {
      // Require the file
      plugin = this.loaded_apps[app_name] = require (app_path);
    } catch (e) {
      console.log ("Error while loading app", app_name, ". It does probably not exist.");
      res.writeHead(500, {"X-ErrorLoading-For": app_name});
      res.end(JSON.stringify ({ "status": 500, "error": "could-not-load-app",
                                "app": app_name, "stack": config.debug && e.stack }));
      return;
    }


    var subpath = this.utils.prefix + "/" + app_name;
    // Get the router path
    if (config.apps[app_name]) {
      subpath = config.apps[app_name].url || subpath;
    }

    /**
     * @param {string} svcname
     */
    var request_service = function (svcname) {
      if (svcname in this.services) {
        return when (this.services[svcname]);
      } else {
        return this.load_service (svcname);
      }
    }.bind (this);

    var fsetroutes = function () {
      // Initialize the plugin and set the routes up
      plugin.initialize (config.apps[app_name], this, subpath, request_service);

      callback (subpath);
    };

    var subrouter = router.path (subpath, fsetroutes);
  };

  Plugin.prototype.set_routes = function (router) {
    var plugin = this;
    this.router = router;

    var init_app = function (appname, rest) {
      rest = rest ? "/" + rest : "";

      console.log ("CALLED INIT OVER ", appname);

      var on_loaded = function (subpath)
      {
        console.log ("I'm here");
        this.res.writeHead(
          302,
          { "X-Application-Loaded": appname,
            "Location": subpath + rest}
        );
        this.res.end(JSON.stringify(
          { "status": 200,
            "app": appname,
            "path": subpath + rest}
        ));
      }.bind(this);

      plugin.load_app (appname, router, this.res, on_loaded);
    };

    router.get ('/_init/:appname', init_app);
    router.get ('/_init/:appname/(.*)', init_app);

  };

  Plugin.prototype.not_found = function (req, res, redirect) {
    var requested_app = req.url.slice (this.utils.prefix.length + 1);

    if (requested_app.slice (0, requested_app.indexOf('/')) in this.loaded_apps) {
      res.end(JSON.stringify({"status": 404, "url": req.url}));
      return {};
    }

    var redirection = this.utils.prefix + "/_init/" + requested_app;

    return redirect (redirection);

    // We will try to redirect this to the init page
    res.writeHead(302, {"Location": redirection });
    res.end(JSON.stringify({"status": 302, "requested_app": requested_app }));
  };

  var plugin_instance = new Plugin ();

  return {
    initialize: plugin_instance.initialize.bind (plugin_instance),
    set_routes: plugin_instance.set_routes.bind (plugin_instance),
    not_found: plugin_instance.not_found.bind (plugin_instance)
  };

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
