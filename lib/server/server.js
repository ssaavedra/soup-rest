/*jslint node: true */
/** @class */

(function (define) { 'use strict';
define(function (require) {
  var _ = require('underscore');
  var config = require ('config'),
      context = require('../context'),
      director = require('director'),
      http = require('http'),
      exec = require ('child_process').exec,
      when = require('when'),
      de = require('director-explorer');


  var RestServer = function RestServer () {
    this.router = new director.http.Router({
      '/dir': this.showRoutes()
    });

    this.router.configure({
      notfound: this.not_found_dispatch ()
    });

    this.not_found_dispatchers = {};

    if (module.parent === null || module.id === '.') {
      this.serve_forever ();
    }
  };

  RestServer.prototype = Object.create (Object.prototype);

  RestServer.prototype.init = function (config) {
    var server = this;
    this.port = config.get('port');

    if (config.has ('port_randomize') && config.get('port_randomize')) {
      this.port_randomize ();
    }

    this.routes = [];
    this.plugins = [];
    this.services = [];

    this.plugins = config.get("plugins");

    _.forEach (this.plugins, function (route_prefix, plugin_name) {
      server.load_plugin (plugin_name, route_prefix);
    });

  };

  RestServer.prototype.port_randomize = function () {
    this.port = 8139; // TODO: Really randomize
  };

  RestServer.prototype.not_found_dispatch = function () {
    var server = this;

    var redirector = function (dispatcher, new_url) {
      if (this.req.REDIRECTING) {
        console.log ("REDIRECTING TRUE", this.req.url, new_url);
        return dispatcher (this.req.url);
      }

      console.log ("REDIRECTING FALSE");
      this.req.REDIRECTING = true;

      this.req.url = new_url;

      return server.router.dispatch (this.req, this.res, function (err) {});
    };

    return function (callback) {
      for (var prefix in server.not_found_dispatchers) {
        if (server.not_found_dispatchers.hasOwnProperty(prefix)) {
          if (this.req.url.slice(0, prefix.length) == prefix) {
            return server.not_found_dispatchers[prefix].call (this,
                                                              this.req,
                                                              this.res,
                                                              redirector.bind (this, callback));
          } else {
            console.log (this.req.url.slice(0, prefix.length), "!=", prefix);
          }
        }
      }

      return callback (null, this.req, this.res);

      this.res.end("Route " + this.req.url + " not found");
    }
  };

  /**
   * Loads a plugin into the REST server.
   *
   * @param {string} plugin_name
   * @param {string} route_prefix
   */
  RestServer.prototype.load_plugin = function (plugin_name, route_prefix) {
    // if plugin starts with $, replace it with dir
    if (plugin_name[0] == "$") {
      plugin_name = __dirname + plugin_name.slice (1);
    }

    // Load it!
    console.log ("LOADING PLUGIN ", plugin_name);
    var plugin = require (plugin_name);

    if (plugin.initialize)
      plugin.initialize({ prefix: route_prefix,
                          return_json: this.return_json,
	                  jsonify: this.jsonify.bind (this),
	                  error500: this.error500,
                          load_service: this.$load_service.bind (this)
			});
    if (plugin.not_found)
      this.not_found_dispatchers[route_prefix] = plugin.not_found.bind (plugin);

    var fsetroutes = function () {
      return plugin.set_routes (this);
    };

    this.router.path (route_prefix, fsetroutes);
  };

  RestServer.prototype.$load_service = function (svc_name) {
    var q = when.defer ();

    if (config.http.safe_plugins && config.http.safe_plugins.indexOf (svc_name) == -1) {
      q.reject (new TypeError("Service not found or forbidden"));
      return q.promise;
    }

    context.getProto (svc_name, function (s) {
        q.resolve (s);
    });

    return q.promise;
  }

  /**
   * @returns {when.Promise} with list of IPs
   */
  RestServer.prototype.get_ips = function () {
    var p = when.defer ();
    exec('ifconfig', function (err, stdout, stderr) {
      var ips = [];

      var ips = stdout.split("\n").filter(function (line) {
        return line.match (/inet addr:/);
      }).map  (function (line) {
        var match = line.match (/^.*inet addr:([0-9.]+)\s+.*$/);
        return match[1];
      });

      p.resolve (ips);
    });

    return p.promise;
  };

  RestServer.prototype.serve_forever = function () {
    var self = this, p = when.defer ();

    http.createServer(function (req, res) {
      try {
        req.chunks = [];
        req.on('data', function (chunk) {
          req.chunks.push(chunk.toString());
        });

        self.router.dispatch (req, res, function (err) {
	  if (err) {
	    res.writeHead (404);
	    res.end ('Cannot get resource ' + err);
	  }
        });
      } catch (e) {
        console.log ("Error catched");
        self.error500.call ({req: req, res: res}, e);
      }
    }).listen (this.port);

    this.get_ips().then (function (port, ips) {
      ips.forEach (function (port, ip) {
        console.log ("Listening in http://" + ip + ":" + port);
      }.bind (this, port));

      p.resolve (ips);

    }.bind (this, this.port));
    this.ip = "";

    console.log ("Listening in: http://127.0.0.1:" + this.port);

    return p.promise;
  };

  RestServer.prototype.showRoutes = function () {
    return (function (router) {
      return function () {
	var rsp = 'Available methods:\n';

	rsp += de.table(router);
	this.res.end(rsp);
      };
    }(this.router));
  };

  RestServer.prototype.return_json = function (res, status, content) {
    var content_json = JSON.stringify(content);
    content_json += "\n";
    res.writeHead(status, { 'Content-Type': 'application/json' });
    res.end(content_json);
    return content_json;
  };

  /**
   * Returns a function that has only one callback for content.
   * Can be used as the then part in a promise.
   */

  RestServer.prototype.jsonify = function (res, status_code) {
    var return_json = RestServer.prototype.return_json;

    return function (obj) {
      return return_json (res, status_code, obj);
    };
  };

  RestServer.prototype.error500 = function (exception) {
    // This function should not be bound!

    this.res.writeHead(500, { 'Content-Type': 'application/json' });

    var error = {error: { code: 500, message: "Unexpected behavior" }};

    if (config.debug) {
      error.exception = exception;
    }
    console.log ("Error 500 happened", JSON.stringify(exception));

    this.res.end(JSON.stringify(error));

  };

  return RestServer;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
