/*jslint node: true, ass: true, nomen: true, unparam:true, white: true */
"use strict";

var config = require('config'),
    context = require('../context'),
    url = require('url'),
    db = require('../couchdb-db'),
    Identity = require('../document/identity'), router,
    fs = require('fs');


var main = function () {
  var yargs = require('yargs')
    .usage ('Launch the REST API for SOUPd.\nUsage: $0 [-p PORT]')
    .alias('p', 'port')
    .alias('h', 'help')
    .describe('p', 'Port number to listen for REST requests');

  var argv = yargs.argv;

  if (argv.help) {
    yargs.showHelp (console.error);
    process.exit();
  }

  context.getProto ('restServer', function (server) {
    server.init (config.get('http'));
    server.serve_forever ();
  }, function (e) { console.error ("ERROR. Cannot instantiate", arguments); });

};

main ();
