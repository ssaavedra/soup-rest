if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(['assert', '../base64', 'child_process', 'crypto', 'fs', 'json-stable-stringify', 'when', '../log'], function (assert, base64, cp, crypto, fs, stringify, when, log) {

var logger = log.getLogger ('identity.generate');

exports.keypair = function create_identity_keypair(ok_callback) {
	var pubkey = "", privkey = "";

	var value = when.defer();

	cp.execFile('openssl',  ['genrsa', '2048'], function(err, stdout, stderr) {
		assert.ok(!err);
		privkey = stdout;

		var pubkeyh = cp.spawn('openssl', ['rsa', '-pubout']);
		pubkeyh.on('exit', function(code) {
			assert.equal(code, 0);

			var keypair = {
				pubKey: pubkey,
				privKey: privkey
			};

			value.resolve(keypair);
		});

		pubkeyh.stdout.on('data', function(data) {
			pubkey += data;
		});

		pubkeyh.stdout.setEncoding('ascii');
		pubkeyh.stdin.write(privkey);
		pubkeyh.stdin.end();
	});

	return value.promise;
}

exports.signature = function create_signature_from_doc_keypair(doc, privKey) {
	// Save the key temporarily; we can't use it from memory
	var privkeyfile = './tmp_keypair_';

	var value = when.defer();
	var doc_as_string = stringify(doc);

	fs.writeFile("doc_sign", doc_as_string);

	fs.writeFile(privkeyfile, privKey, function(err) {
		if (err) {
			logger.error ("Error writing file", err);
			value.reject(err);
		} else {
			// Use OpenSSL
			var child = cp.spawn('openssl', ['dgst', '-hex', '-sign', privkeyfile]);
			var signature = '';

			child.on('exit', function(code) {
				if(code != 0)
					value.reject(code);
				assert.equal(0, code);

				// Remove OpenSSL garbage
				signature = signature.replace(/^\(stdin\)=\s*/, '').replace("\n", "");
				// signature = base64.encode(signature);
				// signature = new Buffer(signature, 'hex');
				value.resolve(signature);
			});

			child.stdout.on('data', function(data) {
				signature += data;
			});

			child.stdout.setEncoding('ascii');
			child.stdin.write(doc_as_string);
			child.stdin.end();
		}
	});

	return value.promise;
}

exports.verification = function verify_signature_from_doc_sig_pubkey(doc, sig, pubkey) {
	// Save the key temporarily; we can't use it from memory
	var pubkeyfile = './tmp_pubkey-verify';
	var sigfile = './tmp_sig-verify';

	var value = when.defer();
	var doc_as_string = stringify(doc);

	fs.writeFile("doc_verify", doc_as_string);

	fs.writeFile(pubkeyfile, pubkey, function(err) {
		if (err) {
			logger.error ("Error writing file", err);
			value.reject(err);
		}
		value.resolve();
	});

	return value.promise.then(function() {
		var value = when.defer();
		var raw_sig = new Buffer(sig, 'hex');

		fs.writeFile(sigfile, raw_sig, function(err) {
			if (err) {
				logger.error ("Error writing file", err);
				value.reject(err);
			}
			value.resolve();
		});
		return value.promise;
	}).then(function() {
		// Use OpenSSL
		var child = cp.spawn('openssl', ['dgst', '-verify', pubkeyfile, '-signature', sigfile]);
		var result = '';
		var value = when.defer();

		child.on('exit', function(code) {
			if(code != 0) {
				value.reject(code);
				return;
			}
			if (result == "Verified OK\n")
				value.resolve(result);
			else if (result == "Verification Failure\n")
				value.reject(result);
			else {
				value.reject(result);
			}
		});

		child.stdout.on('data', function(data) {
			result += data;
		});

		child.stdout.setEncoding('ascii');
		child.stdin.write(doc_as_string);
		child.stdin.end();

		return value.promise;
	});
};


	return exports;

});
