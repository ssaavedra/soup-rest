
if (typeof define !== 'function') { var define = require('amdefine')(module); }

define(["config", "then-couchdb"], function (config, couchdb) {

var auth = "",
    proto = "http://";

if (config.has('db.couch.user') && config.has('db.couch.pass')) {
	auth = config.get('db.couch.user') + ":" + config.get('db.couch.pass') + "@";
}

var host = config.get('db.couch.host');
var port = ":" + config.get('db.couch.port');

var url = proto + auth + host + port + "/";

/*

exports.db = couchdb.createClient(config.couch.port, config.couch.host, auth).db;

exports.identities = function() {
	return exports.db(config.identities_db);
};

*/

exports.db = function(db_name) {
	return couchdb.createClient(url + db_name);
};

exports.identities = function() {
	return exports.db(config.get('db.identities_db'));
};

return exports;
});
