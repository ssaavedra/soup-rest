/*jslint node: true, white: true */
"use strict";
// require('amdefine/intercept');

if (typeof define !== 'function') { var define = require('amdefine')(module); }
(function (define) {
define(function (require) {
  var config = require("config"),
      inverted = require("inverted"),
      when = require("when");

  var conf = {
    protos: {
      nodeStorage: {
	module: "./document/storage",
	args: [
	  // "*dbInstance"
	  'soup'
	  // require('./couchdb-db').db('soup') /* TODO: externalize this into a module */
	],
	scope: 'singleton'
      },
      chunkDao: {
	module: "./document/dao/chunk",
	args: [ { db: "*nodeStorage",
		 dht: "*dhtService" }
	      ],
	scope: 'singleton'
      },
      genericDao: {
	module: "./document/dao/generic",
	args: [ { db: "*nodeStorage",
		 dht: "*dhtService" }
	      ],
	scope: 'singleton'
      },
      identityDao: {
	module: "./document/dao/identity",
	args: [ { db: "*nodeStorage",
		 dht: "*dhtService" }
	      ],
	scope: 'singleton'
      },
      claimDao: {
	module: "./document/dao/claim",
	args: [ { db: "*nodeStorage",
		  dht: "*dhtService" }
	      ],
	scope: 'singleton'
      },
      permanodeDao: {
	module: "./document/dao/permanode",
	args: [ { db: "*nodeStorage",
		 dht: "*dhtService" }
	      ],
	scope: 'singleton'
      },
      fileDao: {
	module: "./document/dao/file",
	args: [ { db: "*nodeStorage",
		 dht: "*dhtService" }
	      ],
	scope: 'prototype'
      },

      blobService: {
	module: "./blobService",
	args: [
	  "*chunkDao",
          "*genericDao"
	],
	scope: 'singleton'
      },
      identityService: {
	module: "./identityService",
	args: ["*identityDao" ],
	scope: "singleton"
      },
      claimService: {
	module: "./claimService",
	args: [ { claimDao: "*claimDao",
		  identityService: "*identityService" } ],
	scope: 'singleton'
      },
      permanodeService: {
	module: "./permanodeService",
	args: [ "*permanodeDao",
		"*identityService" ],
	scope: 'singleton'
      },
      fileService: {
	module: "./fileService",
	args: [ { blobService: "*blobService",
		  claimService: "*claimService",
		  fileDao: "*fileDao" } ],
	scope: 'singleton'
      },
      dhtService: {
	module: "./dhtService",
	args: [ "*restServer" ],
	scope: 'singleton'
      },
      restServer: {
	module: "./server/server",
	args: [ ]
      }
    }
  };

  var context = inverted.create(conf, module);
  // context.loader = require;

  return context;

});
})(typeof define === 'function' && define.amd ? define : function (factory) { module.exports = factory(require); });
