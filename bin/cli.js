#!/usr/bin/env node

var repl = require ('repl'),
    context = require ('../lib/context'),
    when = require ('when');

var ctx = {};

var protos = [ "chunkDao",
               "identityDao",
               "claimDao",
               "permanodeDao",
               "fileDao",
               "blobService",
               "claimService",
               "permanodeService",
               "identityService",
               "fileService",
               "dhtService" ];

var q = [];

var assign = function (pname) {
  var d = when.defer ();

  context.getProto (pname, function (p) {
    ctx[pname] = p;
    d.resolve (p);
  });

  return d.promise;
};

for (var i = 0; i < protos.length; i++) {
  q.push (assign (protos[i]));
}

when.all (q).then (function () {
  ctx.getProto = context.getProto.bind (context);

  var user = repl.start ({ prompt: '> ',
			 });

  for (var p in ctx) {
    if (ctx.hasOwnProperty (p)) {
      user.context[p] = ctx[p];
    }
  }

});
