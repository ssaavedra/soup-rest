#!/usr/bin/env node

var util = require('util');
process.listening = true;

var argv  = require('yargs')
            .usage('Usage: $0 -j foo@bar -r kadoh -p azerty')
            .alias('j', 'jid')
            .alias('r', 'resource')
            .alias('p', 'password')
            .argv;

var udp = true;

var debug     = !!argv.debug,
    name      = udp ? 'udpbot'   : 'xmppbot',
    protocol  = udp ? 'jsonrpc2' : 'node_xmlrpc',
    type      = udp ? 'udp'      : 'xmpp',
    config = {
      reactor : {
        protocol  : protocol,
        transport : {
	  "port": 3000
	}
      },
    };

process.env.KADOH_TRANSPORT = type;
var Bootstrap = require(__dirname + '/../lib/dht/dht-bootstrap-kadoh');
var bootstrap = new Bootstrap(null, config);
bootstrap.connect(function() {
  console.log('connected', bootstrap.getAddress());
});
